import os
import ntpath
import glob
import inspect
import numpy as np
import pandas as pd
import networkx as nx
from PIL import Image, ImageEnhance, ImageChops
from plotnine import ggplot, geom_point, aes, geom_line, theme_bw, scale_color_manual, ggtitle
import imageio

from chemscripts import utils
from chemscripts.calc import LocalDriver, Transformator, FileItems, ObjectItems, IRCParse, GaussianCalc, \
        ShortestPathSelector, ChooseSourcesPathSelector, FormchkCalc, Transformation, NBO6Calc, \
        AnyPathSelector, CubeGen, MCubes, METHODS_FORWARDING, MergeDfs
from chemscripts.mylogging import createLogger
from chemscripts.geom import Molecule, Fragment
from chemscripts import nbo
from chemscripts.utils import DEG2RAD
from chemscripts.imageutils import TrimmingBox, HImageLayout, VImageLayout

SCFDIR = './scfcalcs'
PLOTDIR = "./plotting_data"
FRAMEDIR = "./frames"
IRCDIR = './ircs'
MAXPROC = 24

N_HOMO = 5
N_LUMO = 5
MO_IDXS = {
    'dihydro': ["HOMO", "LUMO"],
}
COLORSCHEME = {
        "LUMO": "#73BFF5", # blue
        "HOMO": "#BD88FF", # magenta
        "HOMO-1": "#71FF61", #green
        "HOMO-2": "#EAFA75", #yellow
        "HOMO-3": "#68DEC3", #cyan
        "HOMO-4": "#FFA36F", #orange
        "HOMO-5": "#FF80E3", # pink
        "HOMO-6": "#FF6E7F", # red
        "HOMO-7": "#AAAAAA", # gray
    }

ISOVALUE = 0.1


class IRCParse(Transformation):
    SOURCE_ITEMS = ['irc_logfile']
    TARGET_ITEMS = ['scf_gjf', 'nbo_gjf', 'df_raw_parts']
    NOTE_ITEMS = ['scf_chk', 'nbo_chk']

    AWARE_KEYS = ['molname']
    MERGED_KEYS = []

    # def some_random_name_exec(filename='me2o2'): # Use inspect to parse this one

    def exec(irc_logfile, scf_gjf, nbo_gjf, df_raw_parts, scf_chk, nbo_chk, molname, logger):
        logger.info("Running exec")
        df_dict = {
            'File': [],
            'molname': [],
            'pointtype': [],
            'RxCoord': [],
            'Energy': [],
        }
        
        Transformation.fileitem_assert_simple(irc_logfile)
        ircfile = irc_logfile.get_filename()
        ircdata = utils.parse_irc(ircfile)
        
        pointtype = 'start'
        filename = scf_gjf.get_filename(pointtype=pointtype)
        df_dict['File'].append(ntpath.basename(filename))
        df_dict['molname'].append(molname)
        df_dict['pointtype'].append(pointtype)
        df_dict['RxCoord'].append(0.0)
        df_dict['Energy'].append(ircdata['points']['start_energy'])
        utils.write_gjf(ircdata['points']['start'], ircdata['syms'], 'singlepoint', filename,
                        subs={
                            'nproc': 4,
                            'chkname': ntpath.basename(scf_chk.get_filename(pointtype='start'))
                        })
        scf_gjf.include(filename, pointtype='start')

        nboname = nbo_gjf.get_filename(pointtype='start')
        utils.write_gjf(ircdata['points']['start'], ircdata['syms'], 'nbo', nboname,
                        subs={
                            'nproc': 4,
                            'chkname': ntpath.basename(nbo_chk.get_filename(pointtype='start')),
                            'oldchkname': ntpath.basename(scf_chk.get_filename(pointtype='start')),
                            'file': ntpath.basename(filename).replace('.gjf', ''),
                        })
        nbo_gjf.include(nboname, pointtype='start')

        for path in ['f', 'b']:
            count = 0
            nfiles = 0
            for i in range(len(ircdata['points'][path + '_geoms'])):
                df_dict['molname'].append(molname)
                df_dict['RxCoord'].append(ircdata['points'][path + '_rxcoord'][i])
                df_dict['Energy'].append(ircdata['points'][path + '_energy'][i])
                if count == IRC_SKIP_POINTS:
                    nfiles += 1
                    pointtype = f"{path}{nfiles}"
                    df_dict['pointtype'].append(pointtype)
                    filename = scf_gjf.get_filename(pointtype=pointtype)
                    df_dict['File'].append(ntpath.basename(filename))
                    utils.write_gjf(ircdata['points'][path + '_geoms'][i], ircdata['syms'], 'singlepoint', filename,
                                    subs={
                                        'nproc': 4,
                                        'chkname': ntpath.basename(scf_chk.get_filename(pointtype=pointtype)),
                                    })
                    scf_gjf.include(filename, pointtype=pointtype)

                    nboname = nbo_gjf.get_filename(pointtype=pointtype)
                    utils.write_gjf(ircdata['points'][path + '_geoms'][i], ircdata['syms'], 'nbo', nboname,
                                    subs={
                                        'nproc': 4,
                                        'chkname': ntpath.basename(nbo_chk.get_filename(pointtype=pointtype)),
                                        'oldchkname': ntpath.basename(scf_chk.get_filename(pointtype=pointtype)),
                                        'file': ntpath.basename(filename).replace('.gjf', ''),
                                    })
                    nbo_gjf.include(nboname, pointtype=pointtype)
                    count = 0
                else:
                    df_dict['File'].append(None)
                    df_dict['pointtype'].append(None)
                count += 1
    
        df_raw_parts.include(df_dict)
        logger.info("IRC parsing is finished")



class MOIsosurfGen(MCubes):
    SOURCE_ITEMS = ['mocube']
    TARGET_ITEMS = ['moiso']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    def exec(mocube, moiso, gdriver, logger, isovalue=0.1):
        return MCubes.exec(cube=mocube, iso=moiso.with_renamed_keys("isotype->type"), gdriver=gdriver, logger=logger, isovalue=ISOVALUE)

    def check_if_done(mocube, moiso, gdriver, logger, return_value, isovalue=0.1):
        return MCubes.check_if_done(cube=mocube, iso=moiso, gdriver=gdriver, logger=logger, isovalue=ISOVALUE, return_value=return_value)

    def finalize(mocube, moiso, gdriver, logger, return_value, isovalue=0.1):
        mystuff = moiso.with_renamed_keys("isotype->type")
        MCubes.finalize(cube=mocube, iso=mystuff, gdriver=gdriver, logger=logger, isovalue=ISOVALUE, return_value=return_value)
        moiso += mystuff.with_renamed_keys("type->isotype")


class CubegenMO(CubeGen):
    SOURCE_ITEMS = ['scf_fchk', 'mo_idxs']
    TARGET_ITEMS = ['mocube']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []
METHODS_FORWARDING(CubegenMO,
                   "scf_fchk->fchk  mocube->cube  mo_idxs->idx",
                   baseclass=CubeGen)


class GetMOIdxs(Transformation):
    SOURCE_ITEMS = ['df_full', 'scf_gjf']
    TARGET_ITEMS = ['mo_idxs']
    NOTE_ITEMS = []

    AWARE_KEYS = ['molname', 'pointtype']
    MERGED_KEYS = []

    def exec(df_full, mo_idxs, scf_gjf, molname, pointtype):
        df_object = df_raw.objects[0]['obj']
        
        written = False
        for i in range(len(df_object['File'])):
            if df_object['File'][i] is None or pd.isnull(df_object['File'][i]):
                continue
            if df_object['molname'][i] != molname or df_object['pointtype'][i] != pointtype:
                continue
            assert not written, f"Met the same item in DF twice"

            mos = {}
            for mo_name in MO_IDXS[molname]:
                mos[mo_name] = int(df_object[mo_name + '_idx'][i])
            mo_idxs.include(mos)
            written = True


class TopologySdfGen(Transformation):
    SOURCE_ITEMS = ['nbo6out', 'nbo_log']
    TARGET_ITEMS = ['toposdf']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    def exec(nbo6out, nbo_log, toposdf, logger):
        nbo6out_fname = nbo6out.get_filename()
        nbo3log_fname = nbo_log.get_filename()
        sdfname = toposdf.get_filename()

        logger.info(f"Generating topology from files {nbo6out_fname} and {nbo3log_fname}")
        curmol = Molecule(nbo6log=nbo6out_fname)
        curmol.from_xyz(*utils.parse_gaussian_log(nbo3log_fname))
        curmol.save_sdf(sdfname)
        toposdf.include(sdfname)


class AddMOData(Transformation):
    SOURCE_ITEMS = ['df_raw', 'nbo6out', 'dmmo', 'fmo']
    TARGET_ITEMS = ['df_full', 'df_csv']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = ['molname', 'pointtype']

    def exec(df_raw, df_full, nbo6out, dmmo, fmo, df_csv, logger):
        df_object = df_raw.get_object()
        df_object['Orb_Data'] = []
        for i in range(len(df_object['File'])):
            if df_object['File'][i] != None:
                cur_attrs = {
                    'molname': df_object['molname'][i],
                    'pointtype': df_object['pointtype'][i]
                }

                nbo6out_name = nbo6out.get_filename(**cur_attrs)
                dmmo_name = dmmo.get_filename(**cur_attrs)
                fmo_name = fmo.get_filename(**cur_attrs)

                logger.debug(f"Processing {nbo6out_name}")
                nboobj = nbo.NboCalculation(nbo6out=nbo6out_name, dmmo=dmmo_name, fmo=fmo_name)
                homo_idx, lumo_idx = nbo.get_boundary_indices(nboobj.dmmo.arr)
                mo_energies = np.diag(nboobj.fmo.arr)
                orb_data = {
                    'HOMO_e': mo_energies[homo_idx],
                    'HOMO_idx': int(homo_idx),
                    'LUMO_e': mo_energies[lumo_idx],
                    'LUMO_idx': int(lumo_idx),
                }
                for i in range(N_HOMO):
                    orb_data['HOMO-%d_e' % (i + 1)] = mo_energies[homo_idx - i - 1]
                    orb_data['HOMO-%d_idx' % (i + 1)] = int(homo_idx - i - 1) # INDEXING STARTS FROM 0
                for i in range(N_LUMO):
                    orb_data['LUMO+%d_e' % (i + 1)] = mo_energies[lumo_idx + i + 1]
                    orb_data['LUMO+%d_idx' % (i + 1)] = int(lumo_idx + i + 1)
                df_object['Orb_Data'].append(orb_data)
            else:
                df_object['Orb_Data'].append(None)
        
        logger.info("Formatting dataframe")
        newkeys = []
        for i in range(len(df_object['File'])):
            if df_object['Orb_Data'][i] is not None:
                for key in df_object['Orb_Data'][i].keys():
                    if key not in newkeys:
                        newkeys.append(key)
        
        logger.info("Orbital data keys = " + repr(newkeys))
        for key in newkeys:
            df_object[key] = []
        for i in range(len(df_object['File'])):
            for key in newkeys:
                if df_object['Orb_Data'][i] is None or key not in df_object['Orb_Data'][i]:
                    df_object[key].append(None)
                else:
                    df_object[key].append(df_object['Orb_Data'][i][key])
        del df_object['Orb_Data']
        df_full.include(df_object)

        pd_obj = pd.DataFrame(df_object)
        # pd.set_option('display.max_rows', pd_obj.shape[0]+1)
        # print(repr(pd_obj))
        csv_name = df_csv.get_filename()
        pd_obj.to_csv(csv_name, sep=';')
        df_csv.include(csv_name)
        logger.info("Dataframe was saved in " + csv_name)


class NBO6FullCalc(NBO6Calc):
    SOURCE_ITEMS = ['file47']
    TARGET_ITEMS = ['nbo6out', 'fnbo', 'fmo', 'dmnbo', 'dmmo']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    DO_WAIT = True
    DO_FINALIZE = True
METHODS_FORWARDING(NBO6FullCalc,
                   "nbo6out->out",
                   baseclass=NBO6Calc)


class MergeRawDfs(MergeDfs):
    SOURCE_ITEMS = ['df_raw_parts']
    TARGET_ITEMS = ['df_raw']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = ['molname']
METHODS_FORWARDING(MergeRawDfs,
                   "df_raw_parts->df_parts df_raw->df",
                   baseclass=MergeDfs, methods=['exec'])
        

class ScfFormchkCalc(FormchkCalc):
    SOURCE_ITEMS = ['scf_chk']
    TARGET_ITEMS = ['scf_fchk']
METHODS_FORWARDING(ScfFormchkCalc,
                   "scf_chk->chk scf_fchk->fchk",
                   baseclass=FormchkCalc)


class SCFCalc(GaussianCalc):
    SOURCE_ITEMS = ['scf_gjf']
    TARGET_ITEMS = ['scf_log', 'scf_chk']
METHODS_FORWARDING(SCFCalc,
                   "scf_gjf->gjf scf_log->log scf_chk->chk",
                   baseclass=GaussianCalc)
                   
class NBO3Calc(GaussianCalc):
    SOURCE_ITEMS = ['nbo_gjf', 'scf_chk']
    TARGET_ITEMS = ['nbo_log', 'file47']

    def exec(nbo_gjf, nbo_log, scf_chk, file47, gdriver, logger):
        return GaussianCalc.exec(gjf=nbo_gjf, log=nbo_log, gdriver=gdriver, logger=logger)
    
    def check_if_done(nbo_gjf, nbo_log, scf_chk, file47, return_value, gdriver, logger):
        return GaussianCalc.check_if_done(gjf=nbo_gjf, log=nbo_log, gdriver=gdriver, logger=logger, return_value=return_value)

    def finalize(nbo_gjf, nbo_log, scf_chk, file47, return_value, gdriver, logger):
        file47name_expected = file47.get_filename()
        name = ntpath.basename(file47name_expected).upper()
        dir = os.path.dirname(file47name_expected)
        file47name = os.path.join(dir, name)
        assert os.path.isfile(file47name), f"File {file47name} not found"
        os.rename(file47name, file47name_expected)
        file47.include(file47name_expected)
        GaussianCalc.finalize(gjf=nbo_gjf, log=nbo_log, gdriver=gdriver, logger=logger, return_value=return_value)


def plotting_call():
    import bpy
    import fnmatch
    import pandas as pd
    import sys, os, glob, ntpath
    mycwd = os.getcwd()
    if mycwd not in sys.path:
        sys.path.insert(0, mycwd)

    import chemscripts.utils as utils
    import chemscripts.blender as msblend
    import chemscripts.blender.draw_objects.orbitals as msorbital
    import chemscripts.blender.draw_objects.nboarrows as msarrows

    #SDFNAME = "INSERT HERE"
    #BLEND_FINAL = "INSERT HERE"
    #ISURF_MASK = "INSERT HERE"
    #PNG_MASK = "INSERT HERE"
    #MO_IDX = INSERT HERE
    #NPROC = INSERT HERE
    SMALL_ATOMS = []

    scene = bpy.context.scene

    COLORSCHEME = {
        "LUMO": "#73BFF5", # blue
        "HOMO": "#BD88FF", # magenta
        "HOMO-1": "#71FF61", #green
        "HOMO-2": "#EAFA75", #yellow
        "HOMO-3": "#68DEC3", #cyan
        "HOMO-4": "#FFA36F", #orange
        "HOMO-5": "#FF80E3", # pink
        "HOMO-6": "#FF6E7F", # red
        "HOMO-7": "#AAAAAA", # gray
    }

    for mo_name, mo_idx in MO_IDX.items():
        msblend.cleanup()
        m = msblend.Molecule(small_atoms=SMALL_ATOMS)
        m.read_sdf(SDFNAME)
        m.draw_bonds(caps=False, radius=0.1)
        m.draw_atoms(scale=0.35)

        msorbital.plot_nbo(utils.format_partial(ISURF_MASK, moidx=mo_idx), color=COLORSCHEME[mo_name], reverse=True)

        pngname = PNG_MASK.format(moname=mo_name)
        scene.render.image_settings.file_format = 'PNG'
        scene.render.filepath = pngname
        bpy.context.scene.render.threads = NPROC
        bpy.context.scene.render.threads_mode = 'FIXED'
        bpy.ops.render.render(write_still=1)
        bpy.ops.wm.save_as_mainfile(filepath=BLEND_FINAL.format(moname=mo_name))


class BuildBlenderScript(Transformation):
    SOURCE_ITEMS = ['plotting_sc', 'toposdf', 'mo_idxs']
    TARGET_ITEMS = ['plot_file']
    NOTE_ITEMS = ['toposdf', 'moiso', 'png_frames', 'blend_final']

    AWARE_KEYS = ['molname', 'pointtype']
    MERGED_KEYS = []

    def exec(plotting_sc, plot_file, toposdf, moiso, png_frames, blend_final, mo_idxs, molname, pointtype):
        script_text = plotting_sc.get_object()
        lines = script_text.split('\n')[1:]
        # Remove tabs bc it's not a function anymore
        for i, line in enumerate(lines):
            if len(line) == 0:
                continue
            assert line.startswith(''.join([" "] * 4)), repr(line)
            lines[i] = line[4:]

        # These things will be inserted into code
        toposdf_name = toposdf.get_filename()
        mo_idxs_dict = repr(mo_idxs.get_object())
        png_frames_mask = png_frames.get_filename(**{
            key: "{%s}" % key
                for key in ('moname', 'sign', 'isotype')
        })
        blend_final_mask = blend_final.get_filename(**{
            key: "{%s}" % key
                for key in ('moname', 'sign', 'isotype')
        })
        surf_mask = moiso.get_filename(**{
            key: "{%s}" % key
                for key in ('moidx', 'sign', 'isotype')
        })
        surf_mask = utils.format_partial(surf_mask, isotype="{type}")

        substitutions = {
            'SDFNAME': toposdf_name,
            'BLEND_FINAL': blend_final_mask,
            'ISURF_MASK': surf_mask,
            'PNG_MASK': png_frames_mask,
            'MO_IDX': mo_idxs_dict,
            'NPROC': str(MAXPROC)
        }
        for key, value in substitutions.items():
            for i, line in enumerate(lines):
                if key in line and 'INSERT HERE' in line:
                    lines[i] = lines[i].replace('INSERT HERE', value).replace('#', '')
                    break
        
        filename = plot_file.get_filename(molname=molname, pointtype=pointtype)
        with open(filename, 'w') as f:
            f.write('\n'.join(lines))
        plot_file.include(filename)


class BlenderRun(Transformation):
    SOURCE_ITEMS = ['plot_file', 'blend_start', 'toposdf', 'moiso', 'mo_idxs']
    TARGET_ITEMS = ['blend_final', 'png_frames']
    NOTE_ITEMS = []

    AWARE_KEYS = ['molname', 'pointtype']
    MERGED_KEYS = ['moidx', 'sign', 'isotype']

    DO_WAIT = True
    DO_FINALIZE = True

    def exec(plot_file, blend_start, toposdf, moiso, blend_final, png_frames, mo_idxs, molname, pointtype, gdriver, logger):
        Transformation.fileitem_assert_simple(toposdf)
        Transformation.fileitem_assert_simple(plot_file)
        Transformation.fileitem_assert_simple(blend_start)

        toposdf_name = toposdf.get_filename()
        plot_file_name = plot_file.get_filename()
        blend_start_name = blend_start.get_filename()

        moidx_values = []
        for file, keys in moiso.files.items():
            if keys['moidx'] not in moidx_values:
                moidx_values.append(keys['moidx'])
        final_blends = []
        final_pngs = []
        mo_idxs_obj = mo_idxs.get_object()
        mo_names = {value: key for key, value in mo_idxs_obj.items()}
        for moidx_val in moidx_values:
            final_blends.append(blend_final.get_filename(moname=mo_names[moidx_val]))
            final_pngs.append(png_frames.get_filename(moname=mo_names[moidx_val]))

        jobdata = {
            'plot_py': plot_file_name,
            'start_blend': blend_start_name,
            'final_blends': final_blends,
            'final_pngs': final_pngs,
            'nproc': MAXPROC
        }

        logger.info(f"Queued Blender calc of {toposdf_name}")
        wait_items = gdriver.start_calcs([jobdata], requested_calculator='blender', wait=False)
        return wait_items
    
    def check_if_done(plot_file, blend_start, toposdf, moiso, blend_final, png_frames, mo_idxs, molname, pointtype, gdriver, logger, return_value):
        return gdriver.check_if_done(return_value)

    def finalize(plot_file, blend_start, toposdf, moiso, blend_final, png_frames, mo_idxs, molname, pointtype, gdriver, logger, return_value):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        moidx_values = []
        for file, keys in moiso.files.items():
            if keys['moidx'] not in moidx_values:
                moidx_values.append(keys['moidx'])
        mo_idxs_obj = mo_idxs.get_object()
        mo_names = {value: key for key, value in mo_idxs_obj.items()}
        for moidx_val in moidx_values:
            blend_final.include(blend_final.get_filename(moname=mo_names[moidx_val]), moname=mo_names[moidx_val])
            png_frames.include(png_frames.get_filename(moname=mo_names[moidx_val]), moname=mo_names[moidx_val])


class DfsForPlotting(Transformation):
    SOURCE_ITEMS = ['df_full']
    TARGET_ITEMS = ['df_points']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    def exec(df_full, df_points, logger):
        molnames = []
        df_full_obj = df_full.get_object()
        for molname in df_full_obj['molname']:
            if molname not in molnames:
                molnames.append(molname)
        logger.debug("Found the following moltypes = " + repr(molnames))

        for molname in molnames:
            df_points_obj = pd.DataFrame(df_full_obj)
            df_points_obj = pd.DataFrame(df_points_obj[df_points_obj['molname'] == molname])
            df_points_obj['RxCoord'] = df_points_obj['RxCoord'].transform(lambda x: -x)

            df_points_obj = pd.DataFrame(df_points_obj)
            df_points_obj.drop([
                    key for key in df_points_obj.columns.values if "_idx" in key
                ], axis=1, inplace=True)

            df_points_obj = df_points_obj.sort_values(by='RxCoord', ascending=True).dropna()
            # df_points_obj[]
            df_points_obj['frame_idx'] = np.arange(df_points_obj.shape[0])
            df_points_obj = pd.DataFrame(df_points_obj.melt(id_vars=['File', 'molname', 'pointtype', 'RxCoord', 'frame_idx'],
                              var_name="Type", value_name="E").dropna())
            # pd.set_option('display.max_rows', df_points_obj.shape[0]+1)
            # print(f"Molname = {molname} " + repr(df_points_obj))
        
            df_points.include(df_points_obj, molname=molname)


class TrimBoxesGen(Transformation):
    SOURCE_ITEMS = ['png_frames']
    TARGET_ITEMS = ['trim_boxes']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = ['pointtype']

    def exec(png_frames, trim_boxes, logger):
        maximal_box = TrimmingBox()
        for png_name, keys in png_frames.files.items():
            new_frame = Image.open(png_frames.get_filename(pointtype=keys['pointtype']))
            new_frame.load()
            new_frame = new_frame.resize((int(new_frame.width / 2), int(new_frame.height / 2)))
            maximal_box.extend(new_frame)
        trim_boxes.include(maximal_box)


class PngPostprocess(Transformation):
    SOURCE_ITEMS = ['png_frames', 'trim_boxes']
    TARGET_ITEMS = ['png_proced']
    NOTE_ITEMS = []

    AWARE_KEYS = ['pointtype']
    MERGED_KEYS = []

    def exec(png_frames, trim_boxes, png_proced, pointtype, logger):
        maximal_box = trim_boxes.get_object()
        png_name = png_frames.get_filename()

        new_frame = Image.open(png_name)
        new_frame.load()
        bg = Image.new("RGB", new_frame.size, (255, 255, 255))
        bg.paste(new_frame, mask=new_frame.split()[3])
        enhancer = ImageEnhance.Brightness(bg)
        factor = 1.3
        bg = enhancer.enhance(factor)
        bg = bg.resize((int(bg.width / 2), int(bg.height / 2)))
        bg = bg.crop(maximal_box.points)
        
        newname = png_proced.get_filename()
        bg.save(newname, 'PNG', quality=100)
        png_proced.include(newname)


class PlotMaker(Transformation):
    SOURCE_ITEMS = ['df_points', 'toposdf']
    TARGET_ITEMS = ['png_graphs']
    NOTE_ITEMS = []

    AWARE_KEYS = ['pointtype', 'molname']
    MERGED_KEYS = []

    DO_WAIT = True
    DO_FINALIZE = True

    def exec(df_points, toposdf, png_graphs, molname, pointtype, logger):
        df_points_obj = df_points.get_object()

        ggcolors = {}
        ggcolors['Energy'] = "#000000"
        for linetype in df_points_obj['Type'].unique():
            if linetype in ggcolors:
                continue
            if '_e' in linetype:
                moname = linetype.replace('_e', '')
                if moname in COLORSCHEME:
                    ggcolors[linetype] = COLORSCHEME[moname]
                else:
                    ggcolors[linetype] = "#999999"
            else:
                raise NotImplementedError

        graphname = png_graphs.get_filename()
        subdf = pd.DataFrame(df_points_obj[df_points_obj['pointtype'] == pointtype])
        rxcoord = df_points_obj[df_points_obj['pointtype'] == pointtype].iloc[0]['RxCoord']
        title = "RxCoord = {:.2f}".format(rxcoord)

        wait_items = gdriver.start_calcs([{
            'graphname': graphname,
            'myfunction': 
                lambda : (
                        ggplot() + scale_color_manual(values=ggcolors) + geom_line(df_points_obj, aes(x='RxCoord', y='E', color="Type")) + \
                        geom_point(subdf, aes(x='RxCoord', y='E', color="Type"), size=2) + theme_bw() + ggtitle(title)
                     ).save(filename=graphname, height=8, width=8, units='cm', dpi=230)
        }], requested_calculator='plotnine_graph', wait=False)
        return wait_items

    def check_if_done(df_points, toposdf, png_graphs, molname, pointtype, logger, return_value):
        return gdriver.check_if_done(return_value)

    def finalize(df_points, toposdf, png_graphs, molname, pointtype, logger, return_value):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        png_graphs.include(png_graphs.get_filename())


class FramesPrepare(Transformation):
    SOURCE_ITEMS = ['png_graphs', 'png_proced', 'df_points']
    TARGET_ITEMS = ['final_frames']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    AWARE_ENFORCED = ['pointtype']
    MERGED_KEYS = ['moname']

    def exec(png_graphs, png_proced, final_frames, df_points, pointtype, logger):
        mo_images = {
            keys['moname']: Image.open(png_proced.get_filename(moname=keys['moname']))
                for file, keys in png_proced.files.items()
        }
        for im in mo_images.values():
            im.load()

        def set(layout, mo_type):
            layout.insert(mo_images[mo_type], name="%s_part" % mo_type, type='middle')
    
        mymo = VImageLayout()
        set(mymo, "LUMO")
        set(mymo, "HOMO")

        # leftlay = VImageLayout()
        # midlay = VImageLayout()
        # rightlay = VImageLayout()
        # set(leftlay, "LUMO")
        # set(leftlay, "HOMO")
        # set(leftlay, "HOMO-1")

        # set(midlay, "HOMO-2")
        # set(midlay, "HOMO-3")
        # set(midlay, "HOMO-4")

        # set(rightlay, "HOMO-5")
        # set(rightlay, "HOMO-6")
        # set(rightlay, "HOMO-7")

        # mymo.insert(leftlay.build(), type='middle')
        # mymo.insert(midlay.build(), type='middle')
        # mymo.insert(rightlay.build(), type='middle')

        graph_fname = png_graphs.get_filename()
        graph_im = Image.open(graph_fname)
        graph_im.load()

        # graph_aspratio = graph_im.size[0] / graph_im.size[1]  # width / height
        # graph_im = graph_im.resize((int(maximal_box.height * graph_aspratio), maximal_box.height))
        
        myframe = HImageLayout()
        myframe.insert(mymo.build(), name="MO_part")
        myframe.insert(graph_im, name="Graph_part", type='middle')

        final_im = myframe.build()
        final_im.resize((int(final_im.size[0]/3), int(final_im.size[1]/3)), Image.ANTIALIAS)
        
        df_points_obj = df_points.get_object()
        frameidx = df_points_obj[df_points_obj['pointtype'] == pointtype].iloc[0]['frame_idx']
        frameidx = ("f%5d" % frameidx).replace(' ', '0')
        newname = final_frames.get_filename(frameidx=frameidx)
        final_im.save(newname, 'PNG', quality=50)


class MP4generate(Transformation):
    SOURCE_ITEMS = ['final_frames']
    TARGET_ITEMS = ['final_mp4']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    AWARE_ENFORCED = []
    MERGED_KEYS = ['frameidx']

    def exec(final_frames, final_mp4, logger):
        mp4_filename = final_mp4.get_filename()

        frame_mask = final_frames.get_filename(frameidx=r"f%5d")

        command = f'ffmpeg -y -framerate 2 -i {frame_mask} -c:v libx264 -crf 17 -vf "format=yuv420p,pad=ceil(iw/2)*2:ceil(ih/2)*2" {mp4_filename}'
        os.system(command)
        final_mp4.include(mp4_filename)


class GIFgenerate(Transformation):
    SOURCE_ITEMS = ['final_frames']
    TARGET_ITEMS = ['final_gif']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    AWARE_ENFORCED = []
    MERGED_KEYS = ['frameidx']

    def exec(final_frames, final_gif, logger):
        gjf_filename = final_gif.get_filename()
        frame_mask = final_frames.get_filename(frameidx="*")
        
        files = sorted(list(glob.glob(frame_mask)))
        with imageio.get_writer(gjf_filename, mode='I', format='GIF-PIL') as writer:
            for file in files:
                print("Adding " + file)
                image = imageio.imread(file)
                writer.append_data(image)
        final_gif.include(gjf_filename)

if __name__ == "__main__":
    utils.read_environment("./userenv.json")
    for exe in ("rung", "formchk", "NBO6", "cubegen", "ffmpeg"):
        assert utils.check_availability(exe), "%s is not included in PATH" % exe
    utils.checkout_directory(SCFDIR)
    utils.checkout_directory(PLOTDIR)
    utils.checkout_directory(FRAMEDIR)

    gdriver = LocalDriver(add_calculators=[], maxproc=MAXPROC)
    logger = createLogger("Main")

    transformerA = Transformator(wd=SCFDIR, transformations=[
        IRCParse, SCFCalc, NBO3Calc, ScfFormchkCalc, MergeRawDfs, NBO6FullCalc
    ], logger=logger)
    transformerB = Transformator(wd=SCFDIR, transformations=[
        IRCParse, MergeRawDfs, AddMOData,
        GetMOIdxs, CubegenMO, MOIsosurfGen, TopologySdfGen, BuildBlenderScript, BlenderRun,
    ], logger=logger)
    transformerC = Transformator(wd=SCFDIR, transformations=[
        IRCParse, MergeRawDfs, AddMOData,
        DfsForPlotting, TrimBoxesGen, PngPostprocess, PlotMaker, FramesPrepare,
        MP4generate, GIFgenerate
    ], logger=logger)
    
    irc_logfile = FileItems('irc_logfile', '{calctype}_{molname}.log', wd=IRCDIR)
    irc_logfile.checkout() # calctype='irc'
    transformerA.attempt_include(irc_logfile, required=True)
    transformerB.attempt_include(irc_logfile, required=True)
    transformerC.attempt_include(irc_logfile, required=True)

    scf_gjf  = FileItems('scf_gjf',  '{calctype}_{molname}_geom{pointtype}.gjf')
    scf_log  = FileItems('scf_log',  '{calctype}_{molname}_geom{pointtype}.log')
    scf_chk  = FileItems('scf_chk',  '{calctype}_{molname}_geom{pointtype}.chk')
    scf_fchk = FileItems('scf_fchk', '{calctype}_{molname}_geom{pointtype}.fchk')
    nbo_gjf  = FileItems('nbo_gjf',  '{calctype}_{molname}_geom{pointtype}_nbo.gjf')
    nbo_log  = FileItems('nbo_log',  '{calctype}_{molname}_geom{pointtype}_nbo.log')
    nbo_chk  = FileItems('nbo_chk',  '{calctype}_{molname}_geom{pointtype}_nbo.chk')
    file47   = FileItems('file47',   '{calctype}_{molname}_geom{pointtype}.47')
    nbo6out  = FileItems('nbo6out',  '{calctype}_{molname}_geom{pointtype}.nbo6out')
    fnbo     = FileItems('fnbo',     '{calctype}_{molname}_geom{pointtype}.fnbo')
    fmo      = FileItems('fmo',      '{calctype}_{molname}_geom{pointtype}.fmo')
    dmnbo    = FileItems('dmnbo',    '{calctype}_{molname}_geom{pointtype}.dmnbo')
    dmmo     = FileItems('dmmo',     '{calctype}_{molname}_geom{pointtype}.dmmo')
    mocube   = FileItems('mocube',   '{calctype}_{molname}_geom{pointtype}_mo{moidx}.cube')
    moiso    = FileItems('moiso',    '{calctype}_{molname}_geom{pointtype}_mo{moidx}_{sign}.{isotype}iso')
    toposdf  = FileItems('toposdf',  '{calctype}_{molname}_geom{pointtype}.sdf')
    df_csv   = FileItems('df_csv',   '{calctype}_dataset.csv', wd='.')

    plot_file   = FileItems('plot_file',   'plottingscript_{calctype}_{molname}_geom{pointtype}.py', wd=PLOTDIR)
    blend_start = FileItems('blend_start', 'scene_{calctype}_template.blend', wd='.')
    blend_final = FileItems('blend_final', 'scene_{calctype}_{molname}_geom{pointtype}_mo{moname}.blend', wd=PLOTDIR)
    png_frames  = FileItems('png_frames',  'orbframe_{calctype}_{molname}_geom{pointtype}_mo{moname}.png', wd=PLOTDIR)
    
    png_proced  = FileItems('png_proced',  'orbproced_{calctype}_{molname}_geom{pointtype}_mo{moname}.png', wd=PLOTDIR)
    png_graphs  = FileItems('png_graphs',  'graph_{calctype}_{molname}_geom{pointtype}.png', wd=PLOTDIR)
    final_frames= FileItems('final_frames','frame_{calctype}_{molname}_{frameidx}.png', wd=FRAMEDIR)
    final_mp4   = FileItems('final_mp4','{calctype}_{molname}.mp4', wd='.')
    final_gif   = FileItems('final_gif','{calctype}_{molname}.gif', wd='.')


    df_raw_parts = ObjectItems('df_raw_parts', ['calctype', 'molname'])
    df_raw = ObjectItems('df_raw', ['calctype'])
    df_full = ObjectItems('df_full', ['calctype'])
    mo_idxs = ObjectItems('mo_idxs', ['calctype', 'molname', 'pointtype'])
    plotting_sc = ObjectItems('plotting_sc', ['calctype'])
    df_points = ObjectItems('df_points', ['calctype', 'molname'])
    trim_boxes = ObjectItems('trim_boxes', ['calctype', 'molname', 'moname'])

    for tr in (transformerA, transformerB, transformerC):
        for item in (scf_gjf, scf_log, scf_chk, scf_fchk,
                    nbo_gjf, nbo_log, nbo_chk,
                    file47, nbo6out, fnbo, fmo, dmnbo, dmmo,
                    mocube, moiso, toposdf, df_csv,
                    plot_file, blend_start, blend_final, png_frames,
                    png_graphs, final_frames, final_mp4, final_gif, png_proced):
            tr.attempt_include(item)
        for object in (df_raw_parts, df_raw, df_full, mo_idxs, plotting_sc, df_points, trim_boxes):
            tr.attempt_include(object)
    
    # Initialize 'plotting_sc'
    plotting_source_code = inspect.getsource(plotting_call)
    plotting_sc.objects = [{ 'obj': plotting_source_code, 'keys': {'calctype': 'irc'} }]

    # transformerA.generate('nbo6out', forward={'gdriver': gdriver, 'logger': logger}, quick=True)
    # transformerA.generate('df_raw', forward={'gdriver': gdriver, 'logger': logger}, quick=True)
    # transformerA.generate('scf_fchk', forward={'gdriver': gdriver, 'logger': logger}, quick=True)
    # transformerB.generate('png_frames', forward={'gdriver': gdriver, 'logger': logger}, quick=True)
    # transformerC.generate('final_frames', forward={'gdriver': gdriver, 'logger': logger}, quick=True)
    transformerC.generate('final_mp4', forward={'gdriver': gdriver, 'logger': logger}, quick=True)
                         
                         
                        #  ,# route_selector=ShortestPathSelector) # , )
                        #  route_selector=ChooseSourcesPathSelector(['irc_logfile'], strict=True))
    gdriver.terminate()
