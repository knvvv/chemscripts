import string
import glob
import time
import os
import ntpath
import re
import copy

from chemscripts.mylogging import createLogger
from chemscripts.utils import str_to_type


class ObjectItems:
    def __init__(self, name, keys, logger=None):
        self.name = name
        self.keys = keys

        self.objects = None # Has the structure [{'obj': ObjectItself, 'keys': {key: value}}]
        self.restrictions = None

        if logger is None:
            logger = createLogger("Objects-" + self.name)
        self.logger = logger
    
    def get_keys_list(self):
        return self.keys
    
    def get_values_of_key(self, key):
        res = []
        for item in self.objects:
            kvpairs = item['keys']
            if kvpairs[key] not in res:
                res.append(kvpairs[key])
        # print(f"{key} -> {repr(res)}")
        return res

    def get_restricted(self, **attrs):
        restricted_item = ObjectItems(name=self.name, keys=[key for key in self.keys if key not in attrs], logger=self.logger)
        
        if not self.is_null:
            matching_objects = []
            for item in self.objects:
                object = item['obj']
                keys = item['keys']

                match = True
                for req_key, req_value in attrs.items():
                    if keys[req_key] != req_value:
                        match = False
                        break
                if match:
                    matching_objects.append({
                            'obj': object,
                            'keys': {key: value for key, value in keys.items() if key not in attrs}
                        })
            restricted_item.objects = matching_objects
        for key in attrs.keys():
            assert key in self.keys, f"The key being restricted '{key}' isn't present in the item '{self.name}'"
        restricted_item.restrictions = attrs
        return restricted_item

    def include(self, object, **attrs):
        assert set(attrs.keys()) == set(self.get_keys_list()), \
                f'Not all keys were specified. Passed={list(attrs.keys())}, Expected={self.get_keys_list()}'
        
        if self.is_null:
            self.objects = []
        
        self.objects.append({
            'obj': object,
            'keys': attrs
        })
    
    def get_object(self, **attrs):
        assert self.non_empty, f"Container {self.name} is empty, can not get object. Restrictions = {repr(self.restrictions)}"
        
        matching_objects = []
        for item in self.objects:
            object = item['obj']
            keys = item['keys']

            match = True
            for req_key, req_value in attrs.items():
                if keys[req_key] != req_value:
                    match = False
                    break
            if match:
                matching_objects.append(object)
        assert len(matching_objects) == 1, f"Expected only one object {self.name}. Got {len(matching_objects)}. "\
                                           f"Restrictions = {repr(self.restrictions)}, attrs = {repr(attrs)}"
        return matching_objects[0]

    def with_renamed_keys(self, keys_str):
        raise NotImplementedError
    
    def __iadd__(self, other):
        assert self.name == other.name, \
                f"Merging FileItems with different names: FileItem({repr(self.name)}) += FileItem({repr(other.name)})"
        for item in other.objects:
            self.include(item['obj'], **item['keys'], **other.restrictions)
        return self

    @property
    def is_object(self):
        return True
    
    @property
    def is_file(self):
        return False

    @property
    def is_null(self):
        return self.objects is None
    
    @property
    def is_empty(self):
        return self.objects is not None and len(self.objects) == 0
    
    @property
    def non_empty(self):
        return self.objects is not None and len(self.objects) > 0


class FileItems:
    MASK_DOUBLE_CHECK = True

    def __init__(self, name, filename_mask, wd=None, logger=None):
        self.name = name
        self.filename_mask = filename_mask
        self.wd = wd
        self.files = None

        if logger is None:
            logger = createLogger("Files-" + self.name)
        self.logger = logger
    
    def get_mask(self, **attrs):
        subs = {t[1]: '*' for t in string.Formatter().parse(self.filename_mask) if t[1] is not None}
        for key, value in attrs.items():
            subs[key] = value
        return self.filename_mask.format(**subs)

    def checkout(self, files=None, **attrs):
        search_mask = os.path.join(self.wd, self.get_mask(**attrs))
        
        _files = []
        for file in glob.glob(search_mask):
            _files.append(file) # TODO Record modification time
        
        if files is not None:
            success = True
            for file in files:
                if file not in _files: # TODO Needs to work when self.filename_mask contains subdirectories
                    self.logger.warning(f"File '{file}' was not found")
                    success = False
            if not success:
                raise RuntimeError('Some files are missing')
        
        subs_list = [t[1] for t in string.Formatter().parse(self.filename_mask) if t[1] is not None and t[1] not in attrs]
        # print("subs_list = " + repr(subs_list))

        # Regex for removing self.wd from name
        wd_mask = os.path.join(self.wd, '*')
        wd_regex = re.escape(wd_mask).replace(r"\*", "(.*)")

        # Regex for extracting keys
        pattern = self.get_mask(**attrs)
        regex = re.escape(pattern).replace(r"\*", "(.*)")

        self.files = {}
        for file in _files:
            wd_matches = re.findall(wd_regex, file)
            assert len(wd_matches) != 0 and not(len(wd_matches) > 1)
            pure_name = wd_matches[0]

            assert '*' not in pure_name, "Found * symbol in the name of '{pure_name}'. Don't use it"
            matches = re.findall(regex, pure_name) # TODO Doesn't generate ALL possible matches :((
            assert len(matches) != 0, f"{pure_name} doesn't match the pattern '{pattern}'"
            assert not(len(matches) > 1), f"Several matches found for {pure_name} (has to be only 1). Pattern = '{pattern}'"
            match = matches[0]
            if isinstance(match, str):
                match = (match,)
            
            # Need to check that '_' isn't present in matched parts
            skip_file = False
            for value in match:
                if '_' in value:
                    self.logger.warning(f"File '{pure_name}' doesn't match the pattern for {self.name} (key contains '_')")
                    skip_file = True
            if skip_file:
                continue
            
            newitem = {**attrs} # Forward all preconstrained keys
            abort = False
            for key, value in zip(subs_list, match):
                newitem[key] = str_to_type(value)
            self.files[pure_name] = newitem
        # print(f"Found files {repr(self.files)}")
    
    def get_restricted(self, **attrs):
        subs = {t[1]: "{%s}" % t[1] for t in string.Formatter().parse(self.filename_mask) if t[1] is not None}
        for key, value in attrs.items():
            assert key in subs, f"Key {key} is not present in the mask '{self.filename_mask}'"
            subs[key] = value
        restricted_item = FileItems(name=self.name, filename_mask=self.filename_mask.format(**subs), wd=self.wd, logger=self.logger)

        matching_files = {}
        for file, keys in self.files.items():
            match = True
            for req_key, req_value in attrs.items():
                if keys[req_key] != req_value:
                    match = False
                    break
            if match:
                matching_files[file] = {key: value for key, value in keys.items() if key not in attrs}
        restricted_item.files = matching_files

        mykeys = self.get_keys_list()
        for key in attrs.keys():
            assert key in mykeys, f"The key being restricted '{key}' isn't present in the item '{self.name}'"
        restricted_item.restrictions = attrs

        return restricted_item

    def get_keys_list(self):
        return [t[1] for t in string.Formatter().parse(self.filename_mask) if t[1] is not None]

    def get_values_of_key(self, key):
        res = []
        for kvpairs in self.files.values():
            if kvpairs[key] not in res:
                res.append(kvpairs[key])
        return res

    def get_filename(self, **attrs):
        return os.path.join(self.wd, self.filename_mask.format(**attrs))

    def include(self, filename, **attrs):
        assert set(attrs.keys()) == set(self.get_keys_list()), \
                f'Problem with accepted keys. Passed={list(attrs.keys())}, Expected={self.get_keys_list()}'
        
        # Get rid of 'self.wd' prefix in 'filename'
        wd_pattern = os.path.join(self.wd, '*')
        wd_regex = re.escape(wd_pattern).replace(r"\*", "(.*)")
        matches = re.findall(wd_regex, filename)
        if len(matches) > 0:
            filename = matches[0]

        if self.MASK_DOUBLE_CHECK:
            # Double-check the key-value pairs
            pattern = self.get_mask()
            regex = re.escape(pattern).replace(r"\*", "(.*)")
            matches = re.findall(regex, filename)
            assert len(matches) != 0, f"{filename} doesn't match the pattern '{pattern}'"
            assert not(len(matches) > 1), f"Several matches found for {filename} (has to be only 1). Pattern = '{pattern}'"
            match = matches[0]
            if isinstance(match, str):
                match = (match,)
            
            subs_list = self.get_keys_list()
            parsed_keys = {}
            for key, value in zip(subs_list, match):
                parsed_keys[key] = str_to_type(value)
            okay = True
            for key, value in parsed_keys.items():
                okay = okay and attrs[key] == value
            okay = okay and set(parsed_keys.keys()) == set(attrs.keys())
            assert okay, f"Mismatch between parsed and received keys (filename={filename}): Parsed={repr(parsed_keys)}, Recved={repr(attrs)}"

        fullname = os.path.join(self.wd, filename)
        # while not os.path.isfile(fullname):
        #     time.sleep(1)
        assert os.path.isfile(fullname), f"Included file '{fullname}' not found. (Keys = {repr(attrs)})"

        self.files[filename] = attrs

    def smart_include(self, filename):
        # Get rid of 'self.wd' prefix in 'filename'
        wd_pattern = os.path.join(self.wd, '*')
        wd_regex = re.escape(wd_pattern).replace(r"\*", "(.*)")
        matches = re.findall(wd_regex, filename)
        if len(matches) > 0:
            filename = matches[0]

        # Get key-value pairs from filename
        pattern = self.get_mask()
        regex = re.escape(pattern).replace(r"\*", "(.*)")
        matches = re.findall(regex, filename)
        assert len(matches) != 0, f"{filename} doesn't match the pattern '{pattern}'"
        assert not(len(matches) > 1), f"Several matches found for {filename} (has to be only 1). Pattern = '{pattern}'"
        match = matches[0]
        if isinstance(match, str):
            match = (match,)
        
        subs_list = self.get_keys_list()
        parsed_keys = {}
        for key, value in zip(subs_list, match):
            parsed_keys[key] = str_to_type(value)

        fullname = os.path.join(self.wd, filename)
        assert os.path.isfile(fullname), f"Included file '{fullname}' not found. (Keys = {repr(parsed_keys)})"

        self.files[filename] = parsed_keys

    def __iadd__(self, other):
        if isinstance(other, str):
            self.smart_include(other)
        
        # Else, it has to be FileItem or child class instance
        assert self.name == other.name, \
                f"Merging FileItems with different names: FileItem({repr(self.name)}) += FileItem({repr(other.name)})"
        
        mykeys = self.get_keys_list()
        passed_restrictions = {key: value for key, value in other.restrictions.items() if key in mykeys}
        for filename, keys in other.files.items():
            self.include(filename, **keys, **passed_restrictions)
        return self

    def with_renamed_keys(self, keys_str):
        key_change = {}
        for part in keys_str.split():
            sides = part.split('->')
            key_change[sides[0]] = sides[1]
            
        for key in self.get_keys_list():
            if key not in key_change:
                key_change[key] = key
                
        newmask = self.filename_mask
        for old_key, new_key in key_change.items():
            newmask = newmask.replace('{%s}' % old_key, '{%s}' % new_key)
        res_item = copy.deepcopy(self)
        res_item.filename_mask = newmask

        res_item.files = {}
        for file, keys in self.files.items():
            new_keys = {key_change[key]: value  for key, value in keys.items()}
            res_item.files[file] = new_keys
        return res_item

    @property
    def is_object(self):
        return False
    
    @property
    def is_file(self):
        return True
    
    @property
    def is_null(self):
        return self.files is None
    
    @property
    def is_empty(self):
        return self.files is not None and len(self.files) == 0
    
    @property
    def non_empty(self):
        return self.files is not None and len(self.files) > 0