import multiprocessing, threading, time, os, copy, fnmatch
from chemscripts import utils
from .calculation_types import CALCTYPES_DEFAULT_CLASSES

MAXTRIES = 1
SLEEP_DURATION = 0.1


def get_next_task(todo_files):
    # print(repr(todo_files))
    if len(todo_files) == 0:
        return None
    elif todo_files[0] == "Done":
        return ("Done", 0)
    else:
        return (todo_files[0], todo_files[0]['nproc'])


def gauss_driver(todo_files, todo_lock, done_files, done_lock, calctype_manager, maxproc):
    # import subprocess
    # subprocess.Popen('formchk', shell = True)
    # raise Exception('kek')
    """
        Process attributes:
        'execution' - execution template
        'wd' - where to execute
        'output_files' (dict) - check their existence after termination 
        'nproc' - number of occupied threads
        'wait_id' - some unique identifier of your calculation
        'calculator_class' - type of calculator class
        + whatever you what to substitute into execution template
        Added after start:
        'proc' - subprocess object
    """
    procs = []
    nfiles = 0
    termination_mode = False
    ntries = {}
    occupied_proc = 0
    
    mainwd = os.getcwd()

    with todo_lock:
        next_task = get_next_task(todo_files) # Tuple (filename, nproc)
    while nfiles > 0 or len(procs) > 0 or not termination_mode:
        for i in reversed(range(len(procs))):
            calc_clean = {key: procs[i][key] for key in procs[i] if key != 'proc'}
            calc_class = calctype_manager.calculator_for_object(calc_clean)
            if calc_class.is_finished(procs[i]['proc']):
                if calc_class.is_successful(calc_clean, calc_class):
                    print("Normal termination of " + procs[i]['wait_id'])
                    with done_lock:
                        done_files.append(calc_clean)
                else:
                    print("Not normal termination of " + procs[i]['calculator_class'])
                    # print("The task was '{}'".format(procs[i]['command']))

                    ntries_key = procs[i]['wait_id']
                    if ntries_key not in ntries:
                        ntries[ntries_key] = 1
                    else:
                        ntries[ntries_key] += 1
                    
                    if ntries[ntries_key] <= MAXTRIES:
                        with todo_lock:
                            todo_files.append(calc_clean)
                    else:
                        with done_lock:
                            done_files.append({**calc_clean, 'failed': True})
                occupied_proc -= procs[i]['nproc']
                del procs[i]

        if next_task is None:
            with todo_lock:
                if len(todo_files) > 0:
                    next_task = get_next_task(todo_files)
        
        while next_task is not None and next_task[1] <= maxproc - occupied_proc: #nfiles > 0
            with todo_lock:
                cur_calc = todo_files.pop(0)
                curnproc = next_task[1]
                next_task = get_next_task(todo_files)
            
            # if cur_calc != "Done" and cur_calc['wait_id'] in ntries and ntries[cur_calc['wait_id']] > MAXTRIES:
            #     continue

            if cur_calc == "Done":
                print("Enabled termination mode")
                termination_mode = True
            else:
                os.chdir(cur_calc['wd'])

                calc_class = calctype_manager.calculator_for_object(cur_calc)
                new_thread = calc_class.open_thread(cur_calc, calc_class)
                new_calc = {
                    'proc': new_thread,
                    **cur_calc
                }
                procs.append(new_calc)
                
                os.chdir(mainwd)
                occupied_proc += curnproc
            with todo_lock:
                nfiles = len(todo_files)
        with todo_lock:
                nfiles = len(todo_files)
        time.sleep(SLEEP_DURATION)


class CalctypeManager:
    def __init__(self, add_calculators=[]):
        self.calctype_classes = copy.deepcopy(CALCTYPES_DEFAULT_CLASSES)
        for new_calculator in add_calculators:
            already_exists = False
            for i, item in enumerate(self.calctype_classes):
                if item.CALC_NAME == new_calculator.CALC_NAME:
                    self.calctype_classes[i] = new_calculator
                    already_exists = True
                    break
            if not already_exists:
                self.calctype_classes.append(new_calculator)
        
        self.calctype_names = {
                calctype.CALC_NAME: calctype
                    for calctype in self.calctype_classes
            }
    
    def calculator_for_file(self, filename):
        for my_class in self.calctype_classes:
            if not my_class.AUTOMATIC_FOR_TYPE:
                continue
            
            for mask in my_class.SUPPORTED_MASKS:
                if fnmatch.fnmatch(filename, mask):
                    return my_class
        raise RuntimeError(f"Don't know how to compute {filename}")
    
    def calculator_for_object(self, obj):
            return self.calctype_names[obj['calculator_class']]
    
    def calculator_by_name(self, calcname):
        return self.calctype_names[calcname]


class LocalDriver:
    def __init__(self, maxproc=None, add_calculators=[]):
        if maxproc is None:
            print("nprocs of OS = %d" % os.cpu_count())
            maxproc = os.cpu_count()
            print("Since MAXPROC was not given. Automatically decided to use %d threads." % maxproc)
        assert maxproc <= os.cpu_count(), f"Given maxproc={maxproc} is greater than nthreads in the system ({os.cpu_count}"

        self.calctype_manager = CalctypeManager(add_calculators=add_calculators)
        
        todo_files = []
        todo_lock = multiprocessing.Lock()
        done_files = []
        done_lock = multiprocessing.Lock()
        self.lists = {
                         'todo': todo_files,
                         'done': done_files,
                     }
        self.locks = {
                         'todo': todo_lock,
                         'done': done_lock,
                     }
        thread = threading.Thread(target=gauss_driver, args=(todo_files, todo_lock, 
                                                            done_files, done_lock, 
                                                            copy.deepcopy(self.calctype_manager),
                                                            maxproc))
        thread.start()
        self.thread = thread

    def check_wait_id(self, id):
        with self.locks['done']:
            for item in self.lists['done']:
                if item['wait_id'] == id:
                    return copy.deepcopy(item)
        return None

    def add_job(self, item):
        with self.locks['todo']:
            self.lists['todo'].append(item)

    def terminate(self):
        with self.locks['todo']:
            self.lists['todo'].append("Done")

    def start_calcs(self, job_items, requested_calculator=None, wait=True):
        new_items = []
        if requested_calculator is not None:
            calc_class = self.calctype_manager.calculator_by_name(requested_calculator)
        
        for item in job_items:
            if requested_calculator is None:
                calc_class = self.calctype_manager.calculator_for_file(item)
            new_jobs = calc_class.get_job_data(item, calc_class)
            if isinstance(new_jobs, dict):
                new_jobs = [new_jobs]
            for new_job in new_jobs:
                self.add_job(new_job)
                new_items.append(new_job)
        
        wait_items = []
        for item in new_items:
            wait_items.append(item['wait_id'])
        
        if wait:
            self.wait_for_termination(wait_items)
        else:
            return wait_items
    
    def check_if_done(self, wait_items):
        done = True
        for cur_id in wait_items:
            item = self.check_wait_id(cur_id)
            if item is None:
                done = False
        return done
    
    def check_termination(self, wait_items):
        failed = []
        for cur_id in wait_items:
            item = self.check_wait_id(cur_id)
            if item is not None:
                if item is not None and 'failed' in item and item['failed']:
                    failed.append(item)
        return failed

    def wait_for_termination(self, wait_items):
        wait_items = copy.copy(wait_items)
        failed_calcs = []
        while len(wait_items) != 0:
            for cur_id in wait_items:
                item = self.check_wait_id(cur_id)
                if item is not None:
                    if 'failed' in item and item['failed']:
                        failed_calcs.append(item)
                    wait_items.remove(cur_id)
            time.sleep(1)
        
        if len(failed_calcs) > 0:
            print(f"[ERROR] {len(failed_calcs)} calculations have terminated with errors:")
            for i, item in enumerate(failed_calcs):
                print(f"{i+1}) {item['wait_id']}")
            self.terminate()
            raise RuntimeError("Error termination detected")
