import os
import time
import ntpath
import shutil
import inspect
import pandas as pd
import json

from chemscripts import utils, nbo
from chemscripts import fchkutils

IRC_SKIP_POINTS = 40

NBO_PAIRING = {
        'dmmo': 419,
        'dmnbo': 420,
        'fmo': 699,
        'fnbo': 69,
        'aomo': 228,
        'aonbo': 288,
    }


def METHODS_FORWARDING(mod_class, renamed_items, baseclass, methods=['exec', 'check_if_done', 'finalize']):
    key_change = {}
    for part in renamed_items.split():
        sides = part.split('->')
        key_change[sides[0]] = sides[1] # Maps 'old' -> 'new'
    for method in methods:
        for arg_name in inspect.getfullargspec(getattr(baseclass, method))[0]:
            if arg_name not in key_change:
                key_change[arg_name] = arg_name
    
    if "exec" in methods:
        x = lambda **args : baseclass.exec(**{
                key_change[key]: value
                    for key, value in args.items()
            })
        setattr(mod_class, "exec", x)
    
    if "check_if_done" in methods:
        y = lambda **args : baseclass.check_if_done(**{
                key_change[key]: value
                    for key, value in args.items()
            })
        setattr(mod_class, "check_if_done", y)
    
    if "finalize" in methods:
        z = lambda **args : baseclass.finalize(**{
                key_change[key]: value
                    for key, value in args.items()
            })
        setattr(mod_class, "finalize", z)


class Transformation:
    SOURCE_ITEMS = None
    TARGET_ITEMS = None
    NOTE_ITEMS = None

    AWARE_KEYS = None
    AWARE_ENFORCED = None
    MERGED_KEYS = None


    DO_WAIT = False
    DO_FINALIZE = False
    
    def fileitem_assert_simple(item):
        assert len(item.files) == 1, f"Expected only one {item.name}-file as input. Got {repr(item.files)}"
        item_data = list(item.files.items())[0]
        assert len(item_data[1]) == 0, f"Expected no keys in {item.name}-item. Got {repr(item_data)}"


class MergeDfs(Transformation):
    SOURCE_ITEMS = ['df_parts']
    TARGET_ITEMS = ['df']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    # MERGED_KEYS = specify!!!
    ADD_ARGS = ['logger']


    def exec(df_parts, df, logger):
        logger.info("Running exec")

        df_keys = []
        for item in df_parts.objects:
            for key in item['obj'].keys():
                if key not in df_keys:
                    df_keys.append(key)
        df_dict = {
            key: [] for key in df_keys
        }

        for item in df_parts.objects:
            size = None
            for list in item['obj'].values():
                if size is None:
                    size = len(list)
                else:
                    assert size == len(list), "Mismatch of column sizes"
            
            if size is None:
                continue

            for key in df_dict:
                if key not in item['obj']:
                    df_dict[key] += [None] * size
                else:
                    df_dict[key] += item['obj'][key]
        
        df.include(df_dict)
        logger.info("IRC parsing is finished")


class MCubes(Transformation):
    SOURCE_ITEMS = ['cube']
    TARGET_ITEMS = ['iso']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    DO_WAIT = True
    DO_FINALIZE = True

    def exec(cube, iso, gdriver, logger, isovalue=0.1):
        Transformation.fileitem_assert_simple(cube)

        iso_template = iso.get_filename(sign='{sign}', type='{type}')
        cube_name = cube.get_filename()
        wait_items = gdriver.start_calcs([{
            'output_template': iso_template,
            'myfunction': lambda : nbo.generate_isosurface(cubename=cube_name, meshfile_template=iso_template, ival=isovalue)
        }], requested_calculator='MCubes', wait=False)
        return wait_items

    def check_if_done(cube, iso, gdriver, logger, return_value, isovalue=0.1):
        return gdriver.check_if_done(return_value)

    def finalize(cube, iso, gdriver, logger, return_value, isovalue=0.1):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        for sign in ['plus', 'minus']:
            for isotype in ['vertices', 'edges', 'triangles']:
                iso_fname = iso.get_filename(sign=sign, type=isotype)
                iso.include(iso_fname, sign=sign, type=isotype)


class CubeGen(Transformation):
    SOURCE_ITEMS = ['log', 'fchk', 'idx'] # Can remove log if no reorder (for MOs)
    TARGET_ITEMS = ['cube']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []
    ADD_ARGS = ['gdriver', 'logger']

    DO_WAIT = True
    DO_FINALIZE = True

    def exec(fchk, cube, idx, gdriver, logger, log=None):
        Transformation.fileitem_assert_simple(fchk)
        fchkfile = fchk.get_filename()

        if log is not None:
            Transformation.fileitem_assert_simple(log)
            logfile = log.get_filename()
        else:
            logfile = None

        idx_obj = idx.get_object()
        if isinstance(idx_obj, list):
            idx_list = idx_obj
        elif isinstance(idx_obj, dict):
            idx_list = list(idx_obj.values())
        
        cube_keys = cube.get_keys_list()
        if len(cube_keys) == 1:
            cube_mask = cube.get_filename(**{cube_keys[0]: "{orbidx}"})
        elif len(cube_keys) == 0:
            assert len(idx_list) == 1, f"Cubefile may have no keys only if 1 cubefile is created. idx_list = {repr(idx_list)}"
            cube_mask = cube.get_filename()
        else:
            raise RuntimeError(f"Cubefile is expected to have 1 or 0 key. Got {repr(cube_keys)}")

        jobdata = {
            'fchk': fchkfile,
            'orbidxs': idx_list,
            'log': logfile,
            'do_reorder': logfile is not None,
            'cube': cube_mask
        }

        logger.info(f"Queued Cubegen calc of {fchkfile}")
        wait_items = gdriver.start_calcs([jobdata], requested_calculator='cubegen', wait=False)
        return wait_items

    def check_if_done(fchk, cube, idx, gdriver, logger, return_value, log=None):
        return gdriver.check_if_done(return_value)

    def finalize(fchk, cube, idx, gdriver, logger, return_value, log=None):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        idx_obj = idx.get_object()
        if isinstance(idx_obj, list):
            idx_list = idx_obj
        elif isinstance(idx_obj, dict):
            idx_list = list(idx_obj.values())

        for cur_idx in idx_list:
            idx_key = cube.get_keys_list()[0]
            cube_fname = cube.get_filename(**{idx_key: cur_idx})
            cube.include(cube_fname, **{idx_key: cur_idx})


class NBO6Calc(Transformation):
    SOURCE_ITEMS = ['file47']
    TARGET_ITEMS = ['out', 'fnbo', 'fmo', 'dmnbo', 'dmmo', 'aomo', 'aonbo', 'aomo']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    DO_WAIT = True
    DO_FINALIZE = True
    ADD_ARGS = ['gdriver', 'logger']

    def exec(file47, out, gdriver, logger, fnbo=None, fmo=None, dmnbo=None, dmmo=None, aonbo=None, aomo=None):
        Transformation.fileitem_assert_simple(file47)
        
        requested_matrices = {
            'fnbo': fnbo,
            'fmo': fmo,
            'dmnbo': dmnbo,
            'dmmo': dmmo,
            'aonbo': aonbo,
            'aomo': aomo,
        }
        inputfile = file47.get_filename()
        nbo_keywords = []
        for name, item in requested_matrices.items():
            if item is None:
                continue
            nbo_keywords.append(f"{name.upper()}=W{NBO_PAIRING[name]}") # For example, 'DMMO=W419'
        if len(nbo_keywords) > 0:
            nbo_keywords.append("file=" + ntpath.basename(inputfile).replace('.47', ''))

        # TODO Implement CHOOSE sections
        timetot = 0
        while not os.path.isfile(inputfile) and timetot < 10:
            time.sleep(1)
            timetot += 1
        if os.path.isfile(inputfile):
            print(f"{inputfile} is OKKKKKKK")
        lines = open(inputfile, 'r').readlines()
        lines[1] = ' $NBO {kw} $END\n'.format(
                    kw=' '.join(nbo_keywords)
                )
        if os.path.isdir(os.path.join(os.path.dirname(inputfile), ntpath.basename(inputfile).replace('.47', ''))):
            shutil.rmtree(os.path.join(os.path.dirname(inputfile), ntpath.basename(inputfile).replace('.47', '')))
        os.mkdir(os.path.join(os.path.dirname(inputfile), ntpath.basename(inputfile).replace('.47', '')))
        inputfile = os.path.join(os.path.join(os.path.dirname(inputfile), ntpath.basename(inputfile).replace('.47', '')), ntpath.basename(inputfile))
        logger.error(f"Processing {inputfile}")
        with open(inputfile, 'w') as f:
            f.write(''.join(lines))

        logger.info(f"Queued NBO6 calc of {inputfile}")
        wait_items = gdriver.start_calcs([inputfile], requested_calculator='NBO6', wait=False)
        return wait_items

    def check_if_done(file47, out, gdriver, logger, return_value, fnbo=None, fmo=None, dmnbo=None, dmmo=None, aonbo=None, aomo=None):
        return gdriver.check_if_done(return_value)

    def finalize(file47, out, gdriver, logger, return_value, fnbo=None, fmo=None, dmnbo=None, dmmo=None, aonbo=None, aomo=None):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        file47name = file47.get_filename()
        dir = os.path.join(os.path.join(os.path.dirname(file47name), ntpath.basename(file47name).replace('.47', '')))
        assert os.path.isdir(dir)
        file47name = os.path.join(dir, ntpath.basename(file47name))
        calcname = ntpath.basename(file47name).replace('.47', '')
        actual_name = os.path.join(dir, calcname + "_NBO.out")
        assert os.path.isfile(actual_name), f"File {actual_name} not found"
        expected_name = out.get_filename()
        os.rename(actual_name, expected_name)
        out.include(expected_name)

        requested_matrices = {
            'fnbo': fnbo,
            'fmo': fmo,
            'dmnbo': dmnbo,
            'dmmo': dmmo,
            'aonbo': aonbo,
            'aomo': aomo,
        }
        for name, item in requested_matrices.items():
            if item is None:
                continue
            expected_name = item.get_filename()
            file47name = file47.get_filename()
            actual_name = os.path.join(dir, '{}.{}'.format(calcname, NBO_PAIRING[name]))
            assert os.path.isfile(actual_name), f"File {actual_name} not found"
            os.rename(actual_name, expected_name)
            item.include(expected_name)
        shutil.rmtree(dir)


class FormchkCalc(Transformation):
    SOURCE_ITEMS = ['chk']
    TARGET_ITEMS = ['fchk']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    DO_WAIT = True
    DO_FINALIZE = True
    ADD_ARGS = ['gdriver', 'logger']

    def exec(chk, fchk, gdriver, logger):
        Transformation.fileitem_assert_simple(chk)

        chkfile = chk.get_filename()
        logger.info(f"Queued FormChk calc of {chkfile}")
        wait_items = gdriver.start_calcs([chkfile], requested_calculator='formchk', wait=False)
        return wait_items

    def check_if_done(chk, fchk, return_value, gdriver, logger):
        return gdriver.check_if_done(return_value)

    def finalize(chk, fchk, return_value, gdriver, logger):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        fchk.include(fchk.get_filename())

class GaussianCalc(Transformation):
    SOURCE_ITEMS = ['gjf']
    TARGET_ITEMS = ['log']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    DO_WAIT = True
    DO_FINALIZE = True
    ADD_ARGS = ['gdriver', 'logger']

    def exec(gjf, log, gdriver, logger, chk=None):
        #  len(gjf.files) == 0:
        #     time.sleep(1)
        Transformation.fileitem_assert_simple(gjf)

        gjffile = gjf.get_filename()
        logger.info(f"Queued Gaussian calc of {gjffile}")
        wait_items = gdriver.start_calcs([gjffile], wait=False)
        return wait_items

    def check_if_done(gjf, log, return_value, gdriver, logger, chk=None):
        return gdriver.check_if_done(return_value)

    def finalize(gjf, log, return_value, gdriver, logger, chk=None):
        assert gdriver.check_if_done(return_value)
        failed_calcs = gdriver.check_termination(return_value)
        assert len(failed_calcs) == 0, f"Calculation failed: {repr(failed_calcs)}"

        log.include(log.get_filename())
        if chk is not None:
            chk.include(chk.get_filename())


class GetMOIdx(Transformation):
    SOURCE_ITEMS = ['fchk']
    TARGET_ITEMS = ['moidxs', 'moidxs_json']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    ADD_ARGS = ['logger']

    def exec(fchk, moidxs, logger):
        logger.info(f"reading {fchk.get_filename()}")
        fchk_data = fchkutils.FchkParser(fchk.get_filename(), req_stuff=())
        nelec = fchk_data.nalpha
        norb = fchk_data.nbsuse
        indexing = {}

        for i in range(nelec):
            offset = nelec - i - 1
            indexing[f"HOMO-{offset}"] = i
        for i in range(nelec, norb):
            offset = i - nelec
            indexing[f"LUMO+{offset}"] = i
        
        rename = {
            'HOMO-0': 'HOMO',
            'LUMO+0': 'LUMO',
        }
        for old_key, new_key in rename.items():
            indexing[new_key] = indexing[old_key]
            del indexing[old_key]
        
        # rev = {value: key for key, value in indexing.items()}
        # for i in range(norb):
        #     print(f"{i} - {rev[i]}")
        moidxs.include(indexing)

class SaveJSON(Transformation):
    SOURCE_ITEMS = ['data']
    TARGET_ITEMS = ['file']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    def exec(file, data):
        fname = file.get_filename()
        with open(fname, 'w') as f:
            json.dump(data.get_object(), f)
        file.include(fname)

class ReadJSON(Transformation):
    SOURCE_ITEMS = ['file']
    TARGET_ITEMS = ['data']
    NOTE_ITEMS = []

    AWARE_KEYS = []
    MERGED_KEYS = []

    def exec(file, data):
        fname = file.get_filename()
        with open(fname, 'r') as f:
            data_obj = json.load(f)
        data.include(data_obj)

