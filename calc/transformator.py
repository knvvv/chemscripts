import networkx as nx
import itertools
import inspect
import time
import pandas as pd

from chemscripts.mylogging import createLogger
from .accessgraph import AccessGraph


class AnyPathSelector:
    def select(seqs, logger=None, **attrs):
        if len(seqs) > 0:
            if logger is None:
                logger = createLogger("ShortestPath")
            if len(seqs) > 1:
                logger.warning("Choosing arbitrary valid route")
            return [seqs[0]]
        else:
            return []


class ShortestPathSelector:
    def select(seqs, g, ag, logger=None, **attrs):
        if logger is None:
            logger = createLogger("ShortestPath")
        logger.info("ENTER")
        unique_seqs = []
        def is_extention(a, b): # Checks if 'a' is extension of 'b'
            if len(a) <= len(b):
                return False
            for itemA in a:
                if itemA not in b:
                    return False
            return True
            # b_idx = len(b) - 1
            # for itemA in reversed(a[len(a)-len(b):]):
            #     if b[b_idx] != itemA:
            #         return False
            #     b_idx -= 1
            # return True

        print(repr(seqs))

        min_length = None
        for path in seqs:
            if min_length is None or min_length > len(path):
                min_path = path
                min_length = len(path)
            # logger.info(f"Processing {repr(path)}")
            # delete_idxs = []
            # match = False

            # for trial_i, trial_path in enumerate(unique_seqs):
            #     if is_extention(path, trial_path):
            #         print("MATCH")
            #         match=True
            #     elif is_extention(trial_path, path):
            #         delete_idxs.append(trial_i)
            
            # if not match:
            #     unique_seqs.append(path)
            #     logger.info(f"Added {path}")
            
            # delete_idxs.sort(reverse=True)
            # for idx in delete_idxs:
            #     logger.info(f"Removed {unique_seqs[idx]}")
            #     del unique_seqs[idx]
        return [min_path]


class ChooseSourcesPathSelector:
    def __init__(self, sources, strict=True):
        self.sources = sources
        self.strict = strict
    
    def select(self, seqs, g, ag, logger=None, **attrs):
        if logger is None:
            logger = createLogger("ChosenSources")
        
        chosen_paths = []
        for path in seqs:
            # logger.debug(f"------ Processing path {repr(path)} -------")
            all_ends = ag.get_all_ends(path)
            # logger.debug(f"All ends = {repr(all_ends)}")
            # logger.debug(f"Sources = {repr(self.sources)}")
            if self.strict and set(all_ends) == set(self.sources):
                # logger.debug(f"Route '{repr(path)}' is accepted (strict)")
                chosen_paths.append(path)
            elif not self.strict:
                includes = True
                for item in self.sources:
                    if item not in all_ends:
                        # logger.debug(f"Route '{repr(path)}' is discarded")
                        includes = False
                        break
                if includes:
                    chosen_paths.append(path)
        return chosen_paths


class Transformator:
    def __init__(self, wd, transformations=[], logger=None):
        self.wd = wd
        self.transformations = {t.__name__: t for t in transformations}
        self.graph = nx.DiGraph()

        for transform_node, tr in self.transformations.items():
            for source in tr.SOURCE_ITEMS:
                self.graph.add_edge(source, transform_node, transform=tr)
                self.graph.nodes[source]['is_item'] = True
            for target in tr.TARGET_ITEMS:
                self.graph.add_edge(transform_node, target, transform=tr)
                self.graph.nodes[target]['is_item'] = True
            self.graph.nodes[transform_node]['is_item'] = False
            self.graph.nodes[transform_node]['transform'] = tr
        
        if logger is None:
            logger = createLogger("Transformator")
        self.logger = logger

    def attempt_include(self, item, required=False):
        if not self.graph.has_node(item.name):
            self.graph.add_node(item.name)
            self.graph.nodes[item.name]['is_item'] = True
            assert self.graph.out_degree(item.name) == 0 and self.graph.in_degree(item.name) == 0, "Unexpected node degrees"
            self.logger.warning(f"Including item '{item.name}' that has no corresponding edges")

        if item.is_file:
            if item.wd is None:
                item.wd = self.wd
            if item.is_null:
                item.checkout()
                print(f"Item '{item.name}' after checkout " + repr(item.files))
            assert item.non_empty or not required, f"Required files {repr(item.get_mask())} not found"
        else: # elif item.is_object:
            # Might implement customizable generator method for ObjectItems?
            assert item.non_empty or not required, \
                    f"Required files {repr(item.get_mask())} not found"
        self.graph.nodes[item.name]['item'] = item

    def find_path_to(self, target_name, quick=False, route_selector=None):
        active_graph = nx.Graph()
        active_graph.add_edges_from(self.graph.edges)
        active_graph.add_nodes_from(self.graph.nodes)

        # data = []
        # for node in self.graph.nodes:
        #     data.append({**{'node': node}, **{key: value for key, value in self.graph.nodes[node].items()}})
        # df = pd.DataFrame(data)
        # print(repr(df[df['is_item']]))
        ag = AccessGraph(self.graph, self.graph.nodes, logger=self.logger)
        print(ag.get_state())

        for comp in nx.connected_components(active_graph):
            if target_name in comp:
                main_component = comp
                break
        
        ag = AccessGraph(self.graph, main_component, logger=self.logger)
        if quick:
            found_seqs = []
            for i in ag.paths_iterate(target_name):
                found_seqs.append(i)
                break
        else:
            found_seqs = [path for path in ag.paths_iterate(target_name)]
            if route_selector is not None:
                found_seqs = route_selector.select(seqs=found_seqs, g=self.graph, ag=ag)
        
        assert len(found_seqs) != 0, f"No route to target '{target_name}' was found"
        assert not len(found_seqs) > 1, f"Several paths to the target '{target_name}' were found. {repr(found_seqs)}"

        found_seq = found_seqs[0]

        seq_message = "Sequence of transforms: " + " -> ".join(found_seq)
        self.logger.info("\n\n{sep}\n{message}\n{sep}".format(message=seq_message, sep=''.join(['-']*len(seq_message))))
        return found_seq
    
    def generate(self, target_name, forward={}, quick=False, route_selector=None):
        transform_path = self.find_path_to(target_name, quick=quick, route_selector=route_selector)
        for cur_transform_name in transform_path:
            self.logger.info(f"Starting {cur_transform_name} step")
            cur_transform = self.transformations[cur_transform_name]
            args_list = inspect.getfullargspec(cur_transform.exec)[0] # TODO Separate implementations for restricted args
            if hasattr(cur_transform, 'ADD_ARGS'):
                args_list += cur_transform.ADD_ARGS

            aware_keys = cur_transform.AWARE_KEYS
            if cur_transform.AWARE_ENFORCED is not None:
                aware_enforced_keys = cur_transform.AWARE_ENFORCED
            else:
                aware_enforced_keys = []
            merged_keys = cur_transform.MERGED_KEYS
            assert len(set(aware_keys+aware_enforced_keys).intersection(merged_keys)) == 0, \
                f"Aware and Merge sets overlap. Aware={repr(aware_keys+aware_enforced_keys)}, Merged={repr(merged_keys)}"

            # Construct lists of keys
            keys_lists = {}
            possible_values = {}
            for source_name in cur_transform.SOURCE_ITEMS:
                keys_lists[source_name] = self.graph.nodes[source_name]['item'].get_keys_list()
                for key in keys_lists[source_name]:
                    if key not in possible_values:
                        possible_values[key] = []
            for cur_target_name in cur_transform.TARGET_ITEMS:
                keys_lists[cur_target_name] = self.graph.nodes[cur_target_name]['item'].get_keys_list()
            for note_name in cur_transform.NOTE_ITEMS:
                keys_lists[note_name] = self.graph.nodes[note_name]['item'].get_keys_list()
            self.logger.debug("keys_lists = " + repr(keys_lists))

            # Check that requested keys are present
            aware_okay = True
            for key in aware_keys+aware_enforced_keys:
                if key not in possible_values:
                    aware_okay = False
                    self.logger.error(f"Requested aware key '{key}' is not involved in transform '{cur_transform_name}'")
            merged_okay = True
            for key in merged_keys:
                if key not in possible_values:
                    merged_okay = False
                    self.logger.error(f"Requested merged key '{key}' is not involved in transform '{cur_transform_name}'")
            assert aware_okay and merged_okay, f"Failed key check on the stage '{cur_transform_name}'"
            
            # Check for overlapping naming of keys, items, forwarded args, etc.
            itemnames_set = set()
            keys_set = set()
            forward_stuff = list(forward.keys())
            for l in keys_lists.values():
                keys_set.update(set(l))
            for l in [cur_transform.SOURCE_ITEMS, cur_transform.TARGET_ITEMS, cur_transform.NOTE_ITEMS]:
                itemnames_set.update(set(l))

            def check_overlap(**attrs): # Mandatory: len(attrs) == 2
                names = list(attrs.keys())
                sets = list(attrs.values())
                intersection = sets[0].intersection(sets[1])
                assert len(intersection) == 0, f"{names[0]} and {names[1]} share element(s): {repr(intersection)}"
            check_overlap(item_names=itemnames_set, key_names=keys_set)
            check_overlap(item_names=itemnames_set, forwarded_keys=forward_stuff)
            check_overlap(key_names=keys_set, forwarded_keys=forward_stuff)

            # Check prerequisites for unaware, aware and merged keys
            for key in possible_values.keys():
                each_source = True
                each_target = True
                one_source = False
                one_target = False # "one_target and not one_source" is a criteria for new key
                for source_name in cur_transform.SOURCE_ITEMS:
                    if key in keys_lists[source_name]:
                        one_source = True
                    else:
                        each_source = False
                for cur_target_name in cur_transform.TARGET_ITEMS:
                    if key in keys_lists[cur_target_name]:
                        one_target = True
                    else:
                        each_target = False
                
                if key in aware_keys: # aware
                    assert each_target and one_source, f"Necessary condition for aware key is not satisfied for '{key}'"
                    self.logger.debug(f"Key '{key}' is in aware mode")
                elif key in aware_enforced_keys: # aware
                    assert one_source, f"Necessary condition for aware-enforced key is not satisfied for '{key}'"
                    self.logger.debug(f"Key '{key}' is enforced to aware mode")
                elif key in merged_keys: # merged
                    assert one_source, f"Necessary condition for merged key is not satisfied for '{key}'"
                    self.logger.debug(f"Key '{key}' is in merged mode")
                else: # unaware
                    assert each_source and each_target, f"Necessary condition for unaware key is not satisfied for '{key}'"
                    self.logger.debug(f"Key '{key}' is in unaware mode")
            aware_keys += aware_enforced_keys

            # Generate all possible values of non-merged keys. (New keys in targets are not included)
            for key in merged_keys:
                del possible_values[key]
            for key in possible_values.keys():
                for source_name in cur_transform.SOURCE_ITEMS:
                    if key not in keys_lists[source_name]:
                        continue
                    cur_values = self.graph.nodes[source_name]['item'].get_values_of_key(key)
                    for value in cur_values:
                        if value not in possible_values[key]:
                            possible_values[key].append(value)
            self.logger.debug("possible_values of non-merged keys = " + repr(possible_values))

            target_items = []
            forwarded_args = {key: value for key, value in forward.items() if key in args_list}
            for value_combination in itertools.product(*possible_values.values()):
                restriction = {key: value for key, value in zip(possible_values.keys(), value_combination)}
                # self.logger.debug("Restriction = " + repr(restriction))
                run_attrs = {}
                for item_list in (cur_transform.SOURCE_ITEMS, cur_transform.TARGET_ITEMS, cur_transform.NOTE_ITEMS):
                    for item_name in item_list:
                        run_attrs[item_name] = self.graph.nodes[item_name]['item'].get_restricted(**{
                                key: value for key, value in restriction.items() if key in keys_lists[item_name]
                            })
                
                skip = False
                for item_name in cur_transform.SOURCE_ITEMS:
                    if run_attrs[item_name].is_empty:
                        skip = True
                        break
                if skip:
                    continue

                aware_data = {key: value for key, value in restriction.items() if key in aware_keys}
                args = {**run_attrs, **aware_data}
                return_value = cur_transform.exec(**args, **forwarded_args)

                # Use target_items to hold. Merging here would result in checking out some unfinished calcs.
                target_items.append({
                        'args': args,
                        'restriction': restriction,
                        'return': return_value,
                    })

            # Have to wait for queued calculations to finish before merging and moving on
            if cur_transform.DO_WAIT:
                finished = False
                while not finished:
                    finished = True
                    for item in target_items:
                        if not cur_transform.check_if_done(**item['args'], **forwarded_args, return_value=item['return']):
                            finished = False
                    time.sleep(1)

            # Finalize all calcs
            if cur_transform.DO_FINALIZE:
                for item in target_items:
                    assert cur_transform.check_if_done(**item['args'], **forwarded_args, return_value=item['return'])
                    cur_transform.finalize(**item['args'], **forwarded_args, return_value=item['return'])

            # time.sleep(2)
            # Calcs have finished and now we merge
            for cur_target_name in cur_transform.TARGET_ITEMS:
                for targets in target_items:
                    if targets['args'][cur_target_name].non_empty:
                        self.graph.nodes[cur_target_name]['item'] += targets['args'][cur_target_name]
                    else:
                        self.logger.warning(f"Target '{cur_target_name}' is empty after executing '{cur_transform.__name__}'. "\
                                            f"Restriction={repr(targets['restriction'])}")
            self.logger.info(f"Step {cur_transform_name} finished successfully")
        self.logger.info(f"Target '{target_name}' was generated successfully")
