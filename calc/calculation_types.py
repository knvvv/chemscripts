import os
import ntpath
import string
import subprocess
import threading
from pathlib import Path
import time

from chemscripts import nbo

class GeneralCalculator:
    def check_files(task, cur_class):
        if 'output_files' not in task:
            print(f"Note: Cannot check output files because none are specified for calctype '{cur_class.CALC_NAME}'")
            return True

        # Check if all expected files are present
        files_found = True
        for ftype, files in task['output_files'].items():
            if isinstance(files, str):
                files = [files]
            for file in files:
                if not Path(file).is_file():
                    print(f"[WARNING] {ftype}-file '{file}' was not found")
                    files_found = False
        if not files_found:
            return False
        return True

    def is_successful(task, cur_class):
        if not cur_class.check_files(task, cur_class):
            return False

        if hasattr(cur_class, 'is_normal_termination'):
            return cur_class.is_normal_termination(task, cur_class)
        else:
            print(f"Note: Normal/abnormal termination checks are not implemented for {cur_class.CALC_NAME}")
            return True


class SubprocessCalc(GeneralCalculator):
    def generate_command(task, cur_class):
        keys = [t[1] for t in string.Formatter().parse(task['execution']) if t[1] is not None]
        args = {}
        for key in keys:
            assert key in task, f"The key '{key}' is not specified in job {repr(task)}"
            args[key] = task[key]
        command = task['execution'].format(**args)
        return command

    def open_thread(cur_calc, cur_class):
        command = cur_class.generate_command(cur_calc, cur_class)
        cur_calc['command'] = command
        print(f"Executing '{command}'")
        return subprocess.Popen(command, shell = True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    def is_finished(subproc):
        return not subproc.poll() == None


class FunctionCalc(GeneralCalculator):
    def open_thread(cur_calc, cur_class):
        mythread = threading.Thread(target=cur_calc['myfunction'])
        mythread.start()
        return mythread

    def is_finished(thread):
        return not thread.is_alive()


class PlotnineCalc(FunctionCalc):
    CALC_NAME = 'plotnine_graph'
    AUTOMATIC_FOR_TYPE = False

    def get_job_data(jobitem, cur_class):
        wait_id = jobitem['graphname']
        
        res = {
            'myfunction': jobitem['myfunction'],
            'nproc': 1,
            'output_files': {
                    'graph': jobitem['graphname']
                },
            'wait_id': wait_id,
            'wd': os.getcwd(),
            'calculator_class': cur_class.CALC_NAME,
        }
        return res


class MCubesCalc(FunctionCalc):
    CALC_NAME = 'MCubes'
    AUTOMATIC_FOR_TYPE = False

    def get_job_data(jobitem, cur_class):
        wait_id = jobitem['output_template']
        outfiles = []
        for sign in ('plus', 'minus'):
            for type in ('vertices', 'edges', 'triangles'):
                outfiles.append(jobitem['output_template'].format(sign=sign, type=type))
        res = {
            'myfunction': jobitem['myfunction'],
            'nproc': 1,
            'output_files': {
                    'surf': outfiles
                },
            'wait_id': wait_id,
            'wd': os.getcwd(),
            'calculator_class': cur_class.CALC_NAME,
        }
        return res


class BlenderCalc(SubprocessCalc):
    CALC_NAME = 'blender'
    AUTOMATIC_FOR_TYPE = False

    def get_job_data(job_info, cur_class):
        res = {
            'execution': 'blender {start_blend} -b -P {plot_py}',
            'start_blend': job_info['start_blend'],
            'plot_py': job_info['plot_py'],
            'output_files': {
                    'blend': job_info['final_blends'],
                    'png': job_info['final_pngs'],
                },
            'wd': '.',
            'nproc': job_info['nproc'],
            'calculator_class': cur_class.CALC_NAME,
            'wait_id': "{}_{}".format(job_info['start_blend'], job_info['plot_py']),
        }
        return res
    
    # def is_normal_termination(task, cur_class):
    #     return True

class GaussianCalc(SubprocessCalc):
    CALC_NAME = 'Gaussian'
    AUTOMATIC_FOR_TYPE = True
    SUPPORTED_MASKS = ['*.gjf']

    def get_job_data(filename, cur_class):
        time_spent = 0
        while not os.path.isfile(filename) and time_spent < 10:
            time.sleep(1)
            time_spent += 1
        lines = open(filename, 'r').readlines()
        nproc = 1
        for line in lines:
            if '%nprocs' in line:
                nproc = int(line.split('=')[1].replace('\n', ''))
        
        wait_id = filename
        res = {
            'execution': 'rung {inpfile}',
            'inpfile': ntpath.basename(filename),
            'output_files': {
                    'log': filename.replace('.gjf', '.log')
                },
            'wd': os.path.dirname(filename),
            'nproc': nproc,
            'calculator_class': cur_class.CALC_NAME,
            'wait_id': wait_id,
        }
        return res
    
    def is_normal_termination(task, cur_class):
        loglines = open(task['output_files']['log'], 'r').readlines()
        for line in loglines[-3:]:
            if "Normal termination" in line:
                return True
        return False


class NBO6Calc(SubprocessCalc):
    CALC_NAME = 'NBO6'
    AUTOMATIC_FOR_TYPE = False

    def get_job_data(filename, cur_class):
        wait_id = filename
        res = {
            'execution': 'NBO6 {inpfile}',
            'inpfile': ntpath.basename(filename),
            'output_files': {
                    'out': filename.replace('.47', '_NBO.out')
                },
            'wd': os.path.dirname(filename),
            'nproc': 1,
            'calculator_class': cur_class.CALC_NAME,
            'wait_id': wait_id,
        }
        return res
    
    def is_normal_termination(task, cur_class):
        return os.path.isfile(task['output_files']['out'])

class FormchkCalc(SubprocessCalc):
    CALC_NAME = 'formchk'
    AUTOMATIC_FOR_TYPE = False

    def get_job_data(filename, cur_class):
        wait_id = filename
        res = {
            'execution': 'formchk {inpfile}',
            'inpfile': ntpath.basename(filename),
            'output_files': {
                    'fchk': filename.replace('.chk', '.fchk')
                },
            'wd': os.path.dirname(filename),
            'nproc': 1,
            'calculator_class': cur_class.CALC_NAME,
            'wait_id': wait_id,
        }
        return res
    
    def is_normal_termination(task, cur_class):
        return os.path.isfile(task['output_files']['fchk'])


class CubegenCalc(SubprocessCalc):
    CALC_NAME = 'cubegen'
    AUTOMATIC_FOR_TYPE = False

    def get_job_data(jobdata, cur_class):
        if jobdata['do_reorder']:
            reorder_map = nbo.generate_reorder_map(jobdata['log'], jobdata['orbidxs'])
        else:
            reorder_map = {i: i for i in jobdata['orbidxs']}
        
        jobs = []
        for real_idx, gauss_idx in reorder_map.items():
            cubename = jobdata['cube'].format(orbidx=real_idx)
            assert os.path.dirname(jobdata['fchk']) == os.path.dirname(cubename), \
                   f"Cube '{ntpath.basename(cubename)}' has to be generated in the same dir as {jobdata['fchk']}"
            wait_id = cubename
            newjob = {
                'execution': 'cubegen {nproc} MO={orbidx} {inpfile} {rescube} {quality}',
                'inpfile': ntpath.basename(jobdata['fchk']),
                'orbidx': gauss_idx,
                'rescube': ntpath.basename(cubename),
                'quality': 100,
                'nproc': 1,
                'wd': os.path.dirname(jobdata['fchk']),
                'output_files': {
                        'cube': cubename
                    },
                'calculator_class': cur_class.CALC_NAME,
                'wait_id': wait_id,
            }
            jobs.append(newjob)
        return jobs
    
    def is_normal_termination(task, cur_class):
        return os.path.isfile(task['output_files']['cube'])


class XtbGeneralCalc(SubprocessCalc):
    CALC_NAME = 'XtbGeneral'
    AUTOMATIC_FOR_TYPE = False
    XTB_EXECUTION_TEMPLATE = 'rxtb {inpfile} -P {nproc}'

    def get_job_data(jobitem, cur_class):
        filename = jobitem['filename']
        wait_id = filename

        res = {
            'execution': cur_class.XTB_EXECUTION_TEMPLATE,
            'inpfile': ntpath.basename(filename),
            'output_files': {
                    'log': os.path.join(os.path.dirname(filename), 'LOGFILE')
                },
            'wd': os.path.dirname(filename),
            'nproc': jobitem['nproc'],
            'calculator_class': cur_class.CALC_NAME,
            'wait_id': wait_id,
        }
        return res

    def is_normal_termination(task, cur_class):
        loglines = open(task['output_files']['log'], 'r').readlines()
        for line in loglines[-11:]:
            if "finished run on" in line:
                return True
        return False


CALCTYPES_DEFAULT_CLASSES = [GaussianCalc, XtbGeneralCalc, FormchkCalc, NBO6Calc, CubegenCalc, MCubesCalc, BlenderCalc, PlotnineCalc]
