import glob, fnmatch
from .excelutils import ExcelSheet
from .utils import get_goodvibes_g, get_gaussian_scfener, get_gaussian_freeener, get_orca_freeener, get_orca_scfener


class _Names(object):
    ENERGY_BLOCK = "Energies"
    LOGNAME_COL = "Filename"
    C0_COL = "C0, M"
    SCF_ENERGY_COL = "E, a.u."
    RRHO_ENERGY_COL = "RRHO free energy, a.u."
    QH_ENERGY_COL = "QH free energy, a.u."

ENERGY_PARSE_MAPPING = {
    'gaussian': {
        _Names.SCF_ENERGY_COL: get_gaussian_scfener,
        _Names.RRHO_ENERGY_COL: get_gaussian_freeener,
        _Names.QH_ENERGY_COL: get_goodvibes_g,
    },
    'orca': {
        _Names.SCF_ENERGY_COL: get_orca_scfener,
        _Names.QH_ENERGY_COL: get_orca_freeener,
    }
}


def get_columns(get_scf: bool, get_free: bool, get_rrho: bool):
    res = [_Names.LOGNAME_COL]
    if get_scf:
        res += [_Names.SCF_ENERGY_COL]
    if get_free:
        res += [_Names.C0_COL,
                _Names.QH_ENERGY_COL]
    if get_rrho:
        res += [_Names.RRHO_ENERGY_COL]
    return res

def get_energy_sheet(filemask=None, ignoremask=None, excelsheet=None, get_scf=False, get_free=True, get_rrho=False, ignore_error_term=False, gv_flags=[], calculation='gaussian'):
    assert filemask is not None or excelsheet is not None
    assert not (filemask is not None and excelsheet is not None)
    if filemask is not None:
        excelsheet = ExcelSheet()
        excelsheet.add_block(blockname=_Names.ENERGY_BLOCK,
                             cols=get_columns(get_scf, get_free, get_rrho))

        for file in glob.glob(filemask):
            if ignoremask is not None and fnmatch.fnmatch(file, ignoremask):
                continue
            newitem = {_Names.LOGNAME_COL: file}
            if get_free:
                newitem[_Names.C0_COL] = 1.0
            excelsheet.add_row(blockname=_Names.ENERGY_BLOCK, data=newitem)

    energy_block = excelsheet.block(_Names.ENERGY_BLOCK)
    energy_parsers = ENERGY_PARSE_MAPPING[calculation]
    for item in energy_block['data']:
        kwargs = {
            'ignore_error_term': ignore_error_term,
        }
        if get_free:
            kwargs = {
                **kwargs,
                'gv_flags': gv_flags,
                'conc': item[_Names.C0_COL],
            }
        for energy_type, energy_parser in energy_parsers.items():
            if energy_type in item:
                item[energy_type] = energy_parser(item[_Names.LOGNAME_COL], **kwargs)
    return excelsheet


def energies_to_excel(filemask, excelname): # This is useless
    mysheet = get_energy_sheet(filemask)
    mysheet.save_xlsx(excelname)


def prepare_sheet(filemask, get_scf=False, get_free=True, get_rrho=False):
    excelsheet = ExcelSheet()
    excelsheet.add_block(blockname=_Names.ENERGY_BLOCK,
                         cols=get_columns(get_scf, get_free, get_rrho))
    for file in glob.glob(filemask):
        newitem = {_Names.LOGNAME_COL: file}
        if get_free:
            newitem[_Names.C0_COL] = 1.0
        excelsheet.add_row(blockname=_Names.ENERGY_BLOCK, data=newitem)
    return excelsheet
