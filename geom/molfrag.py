import networkx as nx
import numpy as np
from numpy.linalg import inv, norm
import copy

from ..nbo import NBO3LogParser, NBO6LogParser
from .rmsdpool import RADII


class Molecule:

    def __init__(self, sdf=None, shutup=False, **args):
        if sdf is not None:
            self.readsdf(sdf)
        elif len([key for key in args.keys() if key.startswith('nbo')]) > 0:
            self.readnbotopology(**{
                key: value
                for key, value in args.items() if key.startswith('nbo')
            })  # Must read symbols and xyzs by calling 'from_xyz'
        elif not shutup:
            raise Exception("No args were given")

    def readsdf(self, file):
        with open(file, "r") as f:
            lines = f.readlines()
        natoms = int(lines[3][0:3])
        nbonds = int(lines[3][3:6])
        self.G = nx.Graph()
        for i in range(4, 4 + natoms):
            self.G.add_node(i - 4)
            parts = lines[i].replace("\n", "").split()
            self.G.nodes[i - 4]['xyz'] = np.array(
                [float(parts[0]),
                 float(parts[1]),
                 float(parts[2])])
            self.G.nodes[i - 4]['symbol'] = parts[3]
        for i in range(4 + natoms, 4 + natoms + nbonds):
            at1 = int(lines[i][0:3])
            at2 = int(lines[i][3:7])
            bondtype = int(lines[i][7:10])
            self.G.add_edge(at1 - 1, at2 - 1)
            self.G[at1 - 1][at2 - 1]['type'] = bondtype
        for line in lines:
            if 'M  CHG' in line:
                parts = line.split()[3:]
                charges = [(int(parts[i]), int(parts[i + 1]))
                           for i in range(0, len(parts), 2)]
                for index, charge in charges:
                    self.G.nodes[index - 1]['chrg'] = charge

    def ring_atoms(self):
        temp_graph = nx.Graph()
        temp_graph.add_nodes_from(self.G.nodes)
        temp_graph.add_edges_from(self.G.edges)
        temp_graph.remove_edges_from(nx.bridges(self.G))
        temp_graph.remove_nodes_from([
            node for node in temp_graph.nodes
            if len(list(temp_graph.neighbors(node))) == 0
        ])
        return list(temp_graph.nodes)

    def readnbotopology(self, **attrs):
        assert len(attrs) == 1
        key, logname = list(attrs.items())[0]
        if key == 'nbolog' and logname.endswith('.log') or key == 'nbo3log':
            parser = NBO3LogParser(logname)
        elif key == 'nbolog' and logname.endswith('.out') or key == 'nbo6log':
            parser = NBO6LogParser(logname)
        else:
            raise Exception(NotImplementedError)
        atoms, bonds = parser.get_molecular_topology()
        self.G = nx.Graph()  # TODO What to do with unbonded atoms??
        for atom in atoms:
            self.G.add_node(atom - 1)

        for bond in bonds:
            if self.G.has_edge(bond[0] - 1, bond[1] - 1):
                continue
            self.G.add_edge(bond[0] - 1, bond[1] - 1)
            self.G[bond[0] - 1][bond[1] - 1]['type'] = bonds.count(bond)

    def total_charge(self) -> int:
        res = 0
        for node, keys in self.G.nodes(data=True):
            if 'chrg' in keys:
                res += keys['chrg']
        return res

    def __add__(self, other):
        res = Molecule(shutup=True)
        res.G = nx.Graph()
        maxnode = None
        for node in self.G.nodes:
            res.G.add_node(node)
            res.G.nodes[node]['xyz'] = self.G.nodes[node]['xyz']
            res.G.nodes[node]['symbol'] = self.G.nodes[node]['symbol']
            if maxnode is None or maxnode < node:
                maxnode = node

        for edge in self.G.edges:
            res.G.add_edge(edge[0], edge[1])
            res.G[edge[0]][edge[1]]['type'] = self.G[edge[0]][edge[1]]['type']

        res.idx_map = {}
        for node in other.G.nodes:
            res.idx_map[node] = node + maxnode + 1
        print("Other G has {} nodes".format(repr(list(other.G.nodes))))
        for node in other.G.nodes:
            nodeidx = res.idx_map[node]
            res.G.add_node(nodeidx)
            res.G.nodes[nodeidx]['xyz'] = other.G.nodes[node]['xyz']
            res.G.nodes[nodeidx]['symbol'] = other.G.nodes[node]['symbol']

        for edge in other.G.edges:
            res.G.add_edge(res.idx_map[edge[0]], res.idx_map[edge[1]])
            res.G[res.idx_map[edge[0]]][res.idx_map[
                edge[1]]]['type'] = other.G[edge[0]][edge[1]]['type']
        return res

    def from_xyz(self, xyzs, syms, mult=1.0):
        if hasattr(self, 'G'):
            for i, xyz in enumerate(xyzs):
                self.G.nodes[i]['xyz'] = copy.deepcopy(xyz)
                if 'symbol' in self.G.nodes[i]:
                    assert self.G.nodes[i]['symbol'] == syms[i]
                else:
                    self.G.nodes[i]['symbol'] = syms[i]
        else:
            self.G = nx.Graph()
            for i, xyz in enumerate(xyzs):
                self.G.add_node(i)
                self.G.nodes[i]['xyz'] = copy.deepcopy(xyz)
                self.G.nodes[i]['symbol'] = syms[i]

            for nodeA in range(len(xyzs)):
                for nodeB in range(nodeA):
                    max_dist = mult * (RADII[syms[nodeA]] + RADII[syms[nodeB]])
                    if norm(xyzs[nodeA] - xyzs[nodeB]) < max_dist:
                        self.G.add_edge(nodeA, nodeB)
                        self.G[nodeA][nodeB]['type'] = 1

    def as_xyz(self):
        xyzs = []
        syms = []
        for atom in range(self.G.number_of_nodes()):
            syms.append(self.G.nodes[atom]['symbol'])
            xyzs.append(self.G.nodes[atom]['xyz'])
        return xyzs, syms

    def remove_bond(self, a, b):
        self.G.remove_edge(a - 1, b - 1)

    def has_bond(self, a, b):
        return self.G.has_edge(a - 1, b - 1)

    def xyz(self, i):
        return self.G.nodes[i - 1]['xyz']

    def save_sdf(self, sdfname=None, buffer=None):
        assert (sdfname is not None) ^ (buffer is not None)

        # Charge example "M  CHG  1  21  -1"
        lines = ["", "", ""]
        lines.append("%3d%3d  0  0  0  0  0  0  0  0999 V2000" %
                     (self.G.number_of_nodes(), self.G.number_of_edges()))
        #   -5.5250    1.6470    1.0014 C   0  0  0  0  0  0  0  0  0  0  0  0
        for atom in range(self.G.number_of_nodes()):
            lines.append(
                "%10.4f%10.4f%10.4f%3s  0  0  0  0  0  0  0  0  0  0  0  0" %
                (self.G.nodes[atom]['xyz'][0], self.G.nodes[atom]['xyz'][1],
                 self.G.nodes[atom]['xyz'][2], self.G.nodes[atom]['symbol']))

        for edge in self.G.edges:
            lines.append(
                "%3s%3s%3s  0" %
                (edge[0] + 1, edge[1] + 1, self.G[edge[0]][edge[1]]['type']))

        write_charges = False
        for node in self.G.nodes:
            if 'chrg' in self.G.nodes[node]:
                write_charges = True
                break
        if write_charges:
            charged_atoms = []
            for node in self.G.nodes:
                if 'chrg' not in self.G.nodes[node] or self.G.nodes[node][
                        'chrg'] == 0:
                    continue
                charged_atoms.append((node + 1, self.G.nodes[node]['chrg']))
            charge_strings = [
                f"{index}  {charge}" for index, charge in charged_atoms
            ]
            charge_line = f"M  CHG  {len(charge_strings)}  {'  '.join(charge_strings)}"
            lines.append(charge_line)

        lines.append("M  END\n")

        if sdfname is not None:
            with open(sdfname, "w") as f:
                f.write("\n".join(lines))
        else:
            buffer.write("\n".join(lines))

    def get_bonds(self):
        return list(self.G.edges)

    def add_dummy(self, position):
        idx = self.G.number_of_nodes()
        self.G.add_node(idx)
        self.G.nodes[idx]['xyz'] = position
        self.G.nodes[idx]['dummy'] = True
        # self.G.nodes[idx]['symbol'] = 'He'
        return idx + 1

    def remove_dummies(self):
        for i in range(self.G.number_of_nodes()):
            if 'dummy' in self.G.nodes[i] and self.G.nodes[i]['dummy']:
                self.G.remove_node(i)

    def atom_xyz(self, idx):
        return self.G.nodes[idx - 1]['xyz']

    def keep_atoms(self, idxs):
        for i in range(len(idxs)):
            idxs[i] -= 1
        for node in list(self.G.nodes):
            if node not in idxs:
                self.G.remove_node(node)
        i = 0
        relabel_mapping = {}
        relabel_short = {}
        node_names = sorted(list(self.G.nodes))
        for i, node in enumerate(node_names):
            relabel_mapping[node] = i
            if node in idxs:
                relabel_short[node + 1] = i + 1
        self.G = nx.relabel_nodes(self.G, relabel_mapping, copy=False)
        return relabel_short


class Fragment:

    def __init__(self, mol, startatom, add_atoms=None):
        if startatom is None:
            self.G = mol.G.subgraph(mol.G.nodes)
            self.carried_atoms = list(mol.G.nodes)
        else:
            self.carried_atoms = nx.node_connected_component(
                mol.G, startatom - 1)  # Numbering of atoms starts from 0
            if add_atoms is not None:
                self.carried_atoms.update({i - 1 for i in add_atoms})
            self.G = mol.G.subgraph(self.carried_atoms)
        self.mol = mol

    def build_frame(self, central_atom, dir_atom, plane_atom):
        central_atom -= 1
        dir_atom -= 1
        plane_atom -= 1
        center_xyz = self.mol.G.nodes[central_atom]['xyz']
        dir_xyz = self.mol.G.nodes[dir_atom]['xyz']
        plane_xyz = self.mol.G.nodes[plane_atom]['xyz']

        xv = dir_xyz - center_xyz
        xv /= norm(xv)
        dir2 = plane_xyz - center_xyz
        zv = np.cross(xv, dir2)
        zv /= norm(zv)
        yv = np.cross(zv, xv)

        newframe = np.zeros((4, 4))
        newframe[:3, 0] = xv
        newframe[:3, 1] = yv
        newframe[:3, 2] = zv
        newframe[:3, 3] = center_xyz
        newframe[3, 3] = 1
        return newframe

    def translate_x(self, frame, length):
        translation_matrix = np.array([[1, 0, 0, length], [0, 1, 0, 0],
                                       [0, 0, 1, 0], [0, 0, 0, 1]])
        for atom in self.G.nodes:
            # print("%d) Before " % (atom+1) + repr(self.G.nodes[atom]['xyz']))
            self.G.nodes[atom]['xyz'] = (
                frame @ translation_matrix @ inv(frame) @ np.concatenate(
                    (self.G.nodes[atom]['xyz'], [1]), axis=0))[:3]
            # print("%d) After " % (atom+1) + repr(self.G.nodes[atom]['xyz']))
        # return translation_matrix @ frame

    def rotate_x(self, frame, ang):
        rotmat = np.array([[1, 0, 0, 0], [0, np.cos(ang), -np.sin(ang), 0],
                           [0, np.sin(ang), np.cos(ang), 0], [0, 0, 0, 1]])
        for atom in self.G.nodes:
            # print("%d) Before " % (atom+1) + repr(self.G.nodes[atom]['xyz']))
            self.G.nodes[atom]['xyz'] = (
                frame @ rotmat @ inv(frame) @ np.concatenate(
                    (self.G.nodes[atom]['xyz'], [1]), axis=0))[:3]
            # print("%d) After " % (atom+1) + repr(self.G.nodes[atom]['xyz']))
        # return rotmat @ frame

    def rotate_z(self, frame, ang):
        rotmat = np.array([[np.cos(ang), -np.sin(ang), 0, 0],
                           [np.sin(ang), np.cos(ang), 0, 0], [0, 0, 1, 0],
                           [0, 0, 0, 1]])
        for atom in self.G.nodes:
            # print("%d) Before " % (atom+1) + repr(self.G.nodes[atom]['xyz']))
            self.G.nodes[atom]['xyz'] = (
                frame @ rotmat @ inv(frame) @ np.concatenate(
                    (self.G.nodes[atom]['xyz'], [1]), axis=0))[:3]
            # print("%d) After " % (atom+1) + repr(self.G.nodes[atom]['xyz']))
        # return frame @ rotmat
