
from chemscripts import  utils

import numpy as np

NAMES = {
    'natoms': 'Number of atoms',
    'chrg': 'Charge',
    'mult': 'Multiplicity',
    'nelec': 'Number of electrons',
    'nalpha': 'Number of alpha electrons',
    'nbeta': 'Number of beta electrons',
    'nbasis': 'Number of basis functions',
    'nbsuse': 'Number of independent functions',
    'scfener': 'SCF Energy',
    'normal_term': 'Job Status',
    's2': 'S**2',
    's2ann': 'S**2 after annihilation',
    'moener': 'Alpha Orbital Energies',
    'alpha_moener': 'Alpha Orbital Energies',
    'beta_moener': 'Beta Orbital Energies',
    'aomo': 'Alpha MO coefficients',
    'alpha_aomo': 'Alpha MO coefficients',
    'beta_aomo': 'Beta MO coefficients',
    'dmao': 'Total SCF Density',
}
for key in NAMES.keys():
    NAMES[key] = NAMES[key] + ''.join([' ' for _ in range(43 - len(NAMES[key]))])
BASIC_KEYS = ('natoms', 'chrg', 'mult', 'nelec', 'nalpha', 'nbeta', 'nbasis', 'nbsuse', 'scfener', 'normal_term', 's2', 's2ann')


class FchkParser:
    def __init__(self, fname, req_stuff=('mo', 'dm')):
        lines = open(fname, 'r').readlines()
        self.calctype = lines[1].split()[0]
        assert self.calctype == 'SP', f"{fname} is not a single-point calculation"

        self.data = {}
        for line in lines:
            for key in BASIC_KEYS:
                if NAMES[key] in line:
                    assert key not in self.data, f"Met key '{NAMES[key]}' multiple times in '{fname}'"
                    self.data[key] = utils.str_to_type(line.replace(NAMES[key], '').split()[1])
        assert self.normal_term == 1, f'Abnormal termination of {fname}'

        if 's2' in self.data and 's2ann' in self.data:
            self.restricted = False
        elif 's2' not in self.data and 's2ann' not in self.data:
            self.restricted = True
        else:
            raise RuntimeError("This shouldn't have happened")


        def parse(parser, sizes):
            return parser(lines[i+1:], sizes)
        if 'mo' in req_stuff:
            if self.restricted:
                self.data['moener'] = None
                self.data['aomo'] = None
                for i, line in enumerate(lines):
                    if NAMES['moener'] in line:
                        self.data['moener'] = parse(FchkParser.parse_list, self.nbsuse)
                    elif NAMES['aomo'] in line:
                        self.data['aomo'] = parse(FchkParser.parse_nonsymm_matrix, (self.nbasis, self.nbsuse))
                assert self.data['moener'] is not None
                assert self.data['aomo'] is not None
            else:
                self.data['moener'] = {
                    'alpha': None,
                    'beta': None
                }
                self.data['aomo'] = {
                    'alpha': None,
                    'beta': None
                }

                for i, line in enumerate(lines):
                    if NAMES['alpha_moener'] in line:
                        self.data['moener']['alpha'] = parse(FchkParser.parse_list, self.nbsuse)
                    if NAMES['beta_moener'] in line:
                        self.data['moener']['beta'] = parse(FchkParser.parse_list, self.nbsuse)
                    elif NAMES['alpha_aomo'] in line:
                        self.data['aomo']['alpha'] = parse(FchkParser.parse_nonsymm_matrix, (self.nbasis, self.nbsuse))
                    elif NAMES['beta_aomo'] in line:
                        self.data['aomo']['beta'] = parse(FchkParser.parse_nonsymm_matrix, (self.nbasis, self.nbsuse))

                assert self.data['moener']['alpha'] is not None
                assert self.data['moener']['beta'] is not None
                assert self.data['aomo']['alpha'] is not None
                assert self.data['aomo']['beta'] is not None
        
        if 'dm' in req_stuff:
            self.data['dmao'] = None
            for i, line in enumerate(lines):
                if NAMES['dmao'] in line:
                    self.data['dmao'] = parse(FchkParser.parse_symm_matrix, self.nbasis)
            assert self.data['dmao'] is not None

    @staticmethod
    def parse_symm_matrix(lines, n_elements):
        size = int((np.sqrt(1 + 8*n_elements) - 1) / 2)
        res = np.empty([size, size])
        idxs = {
            'row': 0,
            'col': 0,
        }
        
        def append_elem(newnum, idxs):
            if idxs['col'] > idxs['row']:
                idxs['row'] += 1
                idxs['col'] = 0
            res[idxs['row']][idxs['col']] = newnum
            res[idxs['col']][idxs['row']] = newnum
            idxs['col'] += 1
        
        for line in lines:
            parts = line.split()
            for part in parts:
                append_elem(float(part), idxs)
            if idxs['col'] == size and idxs['row'] == size - 1:
                break
        return res
    
    @staticmethod
    def parse_nonsymm_matrix(lines, sizes):
        assert len(sizes) == 2
        res = np.empty(sizes)
        idxs = {
            'row': 0,
            'col': 0,
        }
        
        def append_elem(newnum, idxs):
            # global cur_row, cur_col, res
            if idxs['row'] == sizes[0]:
                idxs['row'] = 0
                idxs['col'] += 1
            res[idxs['row']][idxs['col']] = newnum
            idxs['row'] += 1
        
        for line in lines:
            parts = line.split()
            for part in parts:
                append_elem(float(part), idxs)
            if idxs['row'] == sizes[0] and idxs['col'] == sizes[1] - 1:
                break
        return res
    
    @staticmethod
    def parse_list(lines, n_elements):
        res = []
        
        for line in lines:
            parts = line.split()
            for part in parts:
                res.append(utils.str_to_type(part))
            if len(res) == n_elements:
                break
        return res
    
    def __getattr__(self, name):
        assert name in self.data
        return self.data[name]
