import numpy as np


def aomo_to_s(aomo):
    # CInv = np.linalg.inv(aomo)
    # S = np.dot(np.transpose(CInv), CInv)
    # np.linalg.lstsq(a, b)
    CInv = np.linalg.pinv(aomo)
    S = np.dot(np.transpose(CInv), CInv)
    return S


def aomo_split(aomo, n_occ):
    # assert aomo.shape[0] == aomo.shape[1]
    NBasis = aomo.shape[0]
    NBsUse = aomo.shape[1]

    A_Occ = np.zeros((NBasis, n_occ))
    A_Virt = np.zeros((NBasis, NBsUse - n_occ))

    for i in range(n_occ):
        A_Occ[:, i] = aomo[:, i]
    for i in range(NBsUse - n_occ):
        A_Virt[:, i] = aomo[:, n_occ + i]

    return A_Occ, A_Virt


def assign_mos(A, B, S):
    D = np.dot(np.transpose(B), np.dot(S, A))

    fail = False
    assignment = {}
    for i in range(D.shape[0]):
        best_j = None
        best_abs = None
        for j in range(D.shape[1]):
            if best_abs is None or abs(D[i][j]) > best_abs:
                best_abs = abs(D[i][j])
                best_j = j
        assert best_j is not None
        assignment[i] = best_j
        # if best_abs < 0.9:
        #     print(f"best = {best_abs} ({repr(D[i])})")
        #     fail = True
    if fail:
        return {i: i for i in range(D.shape[0])}
    else:
        return assignment



def is_occupied(dm_elem):
    if dm_elem > 1.9:
        return True
    elif dm_elem < 0.1:
        return False
    else:
        raise Exception("Unusial MO occupancy!")


def get_boundary_indices(dmmo):
    occ = np.diag(dmmo)
    for i in range(len(occ)):
        if is_occupied(occ[i]) and not is_occupied(occ[i + 1]):
            return i, i + 1
