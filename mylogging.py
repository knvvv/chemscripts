import logging


def create_logger(classname: str, stream=None, filename='mylog.log') -> logging.Logger:
    logger = logging.getLogger(classname)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(name)s:%(levelname)s  %(message)s")

    if filename is not None:
        file_handler = logging.FileHandler(filename)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler(stream)
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)

    logger.addHandler(stream_handler)
    if filename is not None:
        logger.addHandler(file_handler)
        
    return logger
